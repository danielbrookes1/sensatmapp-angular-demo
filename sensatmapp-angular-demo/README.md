# SensatmappAngularDemo
- Daniel Brookes

This app was developed as an angular demo application to display a table of environmental sensors, as specified in the Angular Programming Exercise at https://bitbucket.org/sensatmapp/angular-take-home-test-1.

Design Decisions
- I have utilised NGRX for the state of the application, assuming that the application is likely to grow and factoring out the state allows a simple action-based approach to manipulating the data. The downsides are that there is some fairly hefty boilerplate associated with this approach, possibly less necessary for real demo projects.
- I chose to use Angular Material UI to help get the project up and running quickly, with some basic styling and ux features built in. 
- I have included the moment.js library as I can clearly see that some manipulation of dates and times will be required, however, have not utilised this in the project as it stands.

Assumptions
- The given sensors file was not properly formed JSON. I have manually edited the file to easily read it as JSON, however if this is the real output from the server it would probably be necessary to read the file as text and convert the records into JSON objects separately.
- I have focused on the architecture of the application over performance.
- I have built the application as if it were to be the start of a much larger permanent application (utilising separate state, libraries etc). 
- I have not tested across all browsers. Developed using Chrome v82.0.4389.72.

Future Improvements
- The table takes a while to load (and also) to refresh due to the number of records being built into the DOM. Given more time I would refactor this so that once the inital table data source had been attached I would perform deltas on only the updated information, rather than refreshing the entire table.
- Given more time I would factor out more of the application styles and theming to libraries - I have simply pulled a lot of this together in a hurry to save time. 
- I would complete the unit tests - currently there are several missing and some are simply markers.

Type 'npm run start' to run the application with NPM in a dev server.


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
