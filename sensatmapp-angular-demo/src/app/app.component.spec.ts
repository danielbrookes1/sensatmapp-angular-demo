import { TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { LoggerModule } from 'projects/logger/src/public-api';
import { AppComponent } from './app.component';
import { AppStoreEffects } from './store/app-store.effect';
import { appStoreReducer } from './store/app-store.reducer';

describe('AppComponent', () => {
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        LoggerModule.forRoot({
          environment: 'local'
        }),
        StoreModule.forRoot(appStoreReducer),
        EffectsModule.forRoot([AppStoreEffects])
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'SensatMapp Angular Demo'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('SensatMapp Angular Demo');
  });

});
