import { Action } from '@ngrx/store';

//#region ActionTypes
export enum AppStoreActionTypes {
  INITIALIZE_APP = '[app-store] Initialize App',
  INITIALIZE_APP_SUCCESS = '[app-store] Initialize App Success',
  INITIALIZE_APP_FAIL = '[app-store] Initialize App Fail'
}
//#endregion ActionTypes

//#region InitializeApp
export class InitializeApp implements Action {
  readonly type = AppStoreActionTypes.INITIALIZE_APP;
  constructor() {}
}

export class InitializeAppSuccess implements Action {
  readonly type = AppStoreActionTypes.INITIALIZE_APP_SUCCESS;
  constructor() {}
}

export class InitializeAppFail implements Action {
  readonly type = AppStoreActionTypes.INITIALIZE_APP_FAIL;
  constructor(public payload: { error: any }) {}
}
//#endregion InitializeApp

//#region Actions
export type AppStoreActions =
  InitializeApp
  | InitializeAppSuccess
  | InitializeAppFail;
//#endregion Actions
