import { AppStoreActions, AppStoreActionTypes } from './app-store.action';
import { IAppStoreState, initialAppStoreState } from './app-store.state';

export function appStoreReducer(
  state = initialAppStoreState,
  action: AppStoreActions
): IAppStoreState {
  switch (action.type) {
    case AppStoreActionTypes.INITIALIZE_APP: return { ...state, initializing: true, initialized: false };
    case AppStoreActionTypes.INITIALIZE_APP_SUCCESS: return { ...state, initializing: false, initialized: true };
    case AppStoreActionTypes.INITIALIZE_APP_FAIL: return { ...state, initializing: false, initialized: false };
    
    default: return { ...state, initializing: false, initialized: false };
  }
}
