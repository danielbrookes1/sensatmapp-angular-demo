import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IAppStoreState } from './app-store.state';

export const getAppStoreState = createFeatureSelector<IAppStoreState>('app-store');
