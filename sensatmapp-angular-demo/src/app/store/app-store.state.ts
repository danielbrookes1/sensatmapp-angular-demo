import { EntityAdapter, createEntityAdapter, EntityState } from '@ngrx/entity';
import { createFeatureSelector } from '@ngrx/store';

export interface IAppStoreState {
  initializing: boolean;
  initialized: boolean;
}

export const initialAppStoreState = createFeatureSelector<IAppStoreState>('app-store');
