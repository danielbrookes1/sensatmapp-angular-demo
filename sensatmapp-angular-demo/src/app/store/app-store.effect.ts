import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { of, Observable } from 'rxjs';
import { map, switchMap, catchError, withLatestFrom, filter } from 'rxjs/operators';
import { Store, Action } from '@ngrx/store';
import { LoggerService } from 'projects/logger/src/public-api';

import {
  AppStoreActionTypes,
  InitializeApp,
  InitializeAppSuccess,
  InitializeAppFail
} from './app-store.action';
import { IAppStoreState } from './app-store.state';
import { getMappReadingListLoaded } from 'projects/mapp-reading/src/lib/store/mapp-reading-store.selector';
import { LoadMappReadingList } from 'projects/mapp-reading/src/lib/store/mapp-reading-store.action';
import { IMappReadingStoreState } from 'projects/mapp-reading/src/lib/store/mapp-reading-store.state';

@Injectable()
export class AppStoreEffects {
  namespace: string = 'AppStoreEffects';

  constructor(
    private actions$: Actions,
    private store: Store<IAppStoreState>,
    private mappReadingStore: Store<IMappReadingStoreState>,
    private logger: LoggerService
  ) {}

  //#region InitializeApp
  initializeApp$ = createEffect(() => this.actions$.pipe(
    ofType<InitializeApp>(AppStoreActionTypes.INITIALIZE_APP),
    map(action => {
      this.logger.info(`${this.namespace}.initializeApp$`);
      return new InitializeAppSuccess();
    })
  ));

  initializeAppSuccess$ = createEffect(() => this.actions$.pipe(
    ofType<InitializeAppSuccess>(AppStoreActionTypes.INITIALIZE_APP_SUCCESS),
    withLatestFrom(
      this.mappReadingStore.select(getMappReadingListLoaded),
      (action, mappReadingListLoaded) => ({
        mappReadingListLoaded: mappReadingListLoaded
      })
    ),
    map(payload => {
      this.logger.info(`${this.namespace}.initializeAppSuccess$`);
      if (!(payload.mappReadingListLoaded)) {
        this.mappReadingStore.dispatch(new LoadMappReadingList());
      }
    })
  ), { dispatch: false });
  
  initializeAppFail$ = createEffect(() => this.actions$.pipe(
    ofType<InitializeAppFail>(AppStoreActionTypes.INITIALIZE_APP_FAIL),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.initializeAppFail$`);
      throw new Error(`Failed to initialize the app: ${payload.error.error.message}`);
    })
  ), { dispatch: false });
  //#endregion InitializeApp
  
}
