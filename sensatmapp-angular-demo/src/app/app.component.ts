import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { InitializeApp } from './store/app-store.action';
import { IAppStoreState } from './store/app-store.state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'SensatMapp Angular Demo';
  author: string = 'Daniel Brookes';
  currentYear: number = new Date().getFullYear();
  version: string = '1.0.0';

  constructor(
    private store: Store<IAppStoreState>
  ) {

  }

  ngOnInit() {
    this.store.dispatch(new InitializeApp());
  }
}
