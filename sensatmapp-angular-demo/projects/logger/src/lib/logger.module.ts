import { ModuleWithProviders, NgModule } from '@angular/core';
import { ILoggerConfig } from './model/logger-config.model';
import { ConsoleLoggerService } from './service/console-logger.service';
import { LoggerServiceConfig, LoggerService } from './service/logger.service';

@NgModule({
  declarations: [
    
  ],
  imports: [
    
  ],
  exports: [
    
  ],
  providers: [
    { provide: LoggerService, useClass: ConsoleLoggerService }
  ]
})
export class LoggerModule { 
  static forRoot(config: ILoggerConfig): ModuleWithProviders<LoggerModule> {
    return {
      ngModule: LoggerModule,
      providers: [
        {
          provide: LoggerServiceConfig,
          useValue: config
        }
      ]
    }
  }
}
