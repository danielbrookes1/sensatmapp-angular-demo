import { Inject, Injectable, InjectionToken } from '@angular/core';
import { ILoggerConfig } from '../model/logger-config.model';

export const LoggerServiceConfig = new InjectionToken<ILoggerConfig>("LoggerConfig");

export abstract class Logger {
  environment: string;
  info: any;
  warn: any;
  error: any;

}

@Injectable({
  providedIn: 'root'
})

export class LoggerService implements Logger {
  environment: string;
  info: any;
  warn: any;
  error: any;
  
  constructor(
    @Inject(LoggerServiceConfig) private _config: ILoggerConfig 
  ) {
    console.info(`BmLogger.init | Env: ${_config.environment}`);
    this.environment = _config.environment;
  }

  invokeConsoleMethod(type: string, args?: any): void { }
}

