import { Inject, Injectable } from '@angular/core';
import { ILoggerConfig } from '../model/logger-config.model';
import { LoggerServiceConfig, Logger } from './logger.service';

const noop = (): any => undefined;

@Injectable()
export class ConsoleLoggerService implements Logger {
  environment: string;

  constructor(
    @Inject(LoggerServiceConfig) private _config: ILoggerConfig
  ) {
    console.info(`ConsoleLoggerService.init | Env: ${_config.environment}`);
    this.environment = _config.environment;
  }

  get info() {
    if (this.environment === 'local' || this.environment === 'remote' || this.environment === 'dev') {
      return console.info.bind(console);
    } else {
      return noop;
    }
  }

  get warn() {
    if (this.environment === 'local' || this.environment === 'remote' || this.environment === 'dev') {
      return console.warn.bind(console);
    } else {
      return noop;
    }
  }

  get error() {
    if (this.environment === 'local' || this.environment === 'remote' || this.environment === 'dev') {
      return console.error.bind(console);
    } else {
      return noop;
    }
  }

  invokeConsoleMethod(type: string, args?: any): void {
    const logFn: Function = (console)[type] || console.log || noop;
    logFn.apply(console, [args]);
  }

}
