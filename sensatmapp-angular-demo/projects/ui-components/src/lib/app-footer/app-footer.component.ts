import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './app-footer.component.html',
  styleUrls: ['./app-footer.component.scss'],
  host: {'class': 'app-footer'}
})
export class AppFooterComponent {
  @Input() version: string;
  @Input() author: string;
  @Input() copyright: string = new Date().getFullYear().toString();

  constructor(
    
  ) {}

}
