import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-logo',
  templateUrl: 'app-logo.component.html',
  styleUrls: ['app-logo.component.scss'],
  host: {'class': 'app-logo'}
})
export class AppLogoComponent implements OnInit {
  
  @Input()
  logoUrl: string;

  @Input()
  logoAlt: string;

  @Input()
  logoHeight: number;

  @Input()
  logoWidth: number;

  @Input()
  logoSmUrl: string;

  @Input()
  logoSmAlt: string;

  @Input()
  logoSmHeight: number;

  @Input()
  logoSmWidth: number;

  constructor() {}

  ngOnInit() {}
}
