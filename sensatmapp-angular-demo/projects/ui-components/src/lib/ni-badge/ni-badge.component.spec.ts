import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NiBadgeComponent } from './ni-badge.component';

describe('NiBadgeComponent', () => {
  let component: NiBadgeComponent;
  let fixture: ComponentFixture<NiBadgeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NiBadgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NiBadgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
