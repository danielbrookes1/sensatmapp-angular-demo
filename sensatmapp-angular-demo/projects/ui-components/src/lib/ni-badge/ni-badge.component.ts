import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ni-badge',
  templateUrl: './ni-badge.component.html',
  styleUrls: ['./ni-badge.component.scss']
})
export class NiBadgeComponent implements OnInit {
  @Input() color: string = '';
  @Input() customColor: string = '';
  @Input() outline: boolean = false;
  @Input() borderRadius: boolean = true;
  @Input() arrow: string = '';
  @Input() size: string = '';
  @Input() position: string = '';

  constructor() {}

  ngOnInit() {}

}
