import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'cmp-content-loader',
  templateUrl: 'content-loader.component.html',
  styleUrls: ['content-loader.component.scss'],
  host: {'class.title': 'mytitle'}
})
export class ContentLoaderComponent implements OnInit {
  @Input() title: string;

  constructor() {
    this.title = 'Loading...';
  }

  ngOnInit() {}

}
