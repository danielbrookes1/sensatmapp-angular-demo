import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';;
import { AppHeaderComponent } from './app-header/app-header.component';
import { AppFooterComponent } from './app-footer/app-footer.component';
import { ContentLoaderComponent } from './content-loader/content-loader.component';
import { CustomMaterialModule } from './custom-material/custom-material.module';
import { AppLogoComponent } from './app-logo/app-logo.component';
import { NiAlertComponent } from './ni-alert/ni-alert.component';
import { NiBadgeComponent } from './ni-badge/ni-badge.component';
import { TCAvatarComponent } from './avatar/avatar.component';

@NgModule({
  declarations: [
    ContentLoaderComponent,
    AppHeaderComponent,
    AppFooterComponent,
    AppLogoComponent,
    NiAlertComponent,
    NiBadgeComponent,
    TCAvatarComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    CustomMaterialModule
  ],
  exports: [
    ContentLoaderComponent,
    AppHeaderComponent,
    AppFooterComponent,
    AppLogoComponent,
    NiAlertComponent,
    NiBadgeComponent,
    TCAvatarComponent
  ]
})
export class UiComponentsModule {}
