import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: 'app-header.component.html',
  styleUrls: ['app-header.component.scss'],
  host: {
    '[class.app-navbar]': 'true',
    '[class.show-overlay]': 'showOverlay'
  }
})
export class AppHeaderComponent {
  
  @Input()
  title: string;
  
  @Input()
  openedSidebar: boolean;

  @Input()
  username: string;

  @Input()
  userAvatar: string;

  @Input()
  isGuest: boolean = false;

  @Output()
  sidebarState = new EventEmitter();

  @Output()
  logout: EventEmitter<void> = new EventEmitter<void>();

  showOverlay: boolean;

  constructor(

    ) {
    this.openedSidebar = false;
    this.showOverlay = false;
  }

  openClick(e: Event) {
    e.preventDefault();
    const clickedComponent = (<any>e.target).closest('.nav-item');
    const items = clickedComponent.parentElement.children;
    for (let i = 0; i < items.length; i++) {
      items[i].classList.remove('opened');
    }
    clickedComponent.classList.add('opened');
    this.showOverlay = true;
  }

  closeClick(e: Event) {
    e.preventDefault();
    const clickedComponent = (<any>e.target);
    const items = clickedComponent.parentElement.children;
    for (let i = 0; i < items.length; i++) {
      items[i].classList.remove('opened');
    }
    this.showOverlay = false;
  }

  openSidebarClick() {
    this.openedSidebar = !this.openedSidebar;
    this.sidebarState.emit();
  }

  getInitials(username: string): string {
    return username
      ? ( username.indexOf('@') > -1
          ? username.substr(0, username.indexOf('@'))
          : username
        ).split(/[ ._]/).slice(0, 2).map(o => o.substr(0, 1)).join('')
      : '?';
  }

  logoutClick(e: Event) {
    e.preventDefault();
    e.stopPropagation();
    this.logout.emit();
  }
}
