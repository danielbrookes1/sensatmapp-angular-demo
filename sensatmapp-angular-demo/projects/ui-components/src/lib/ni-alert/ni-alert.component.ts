import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ni-alert',
  templateUrl: './ni-alert.component.html',
  styleUrls: ['./ni-alert.component.scss']
})
export class NiAlertComponent implements OnInit {
  @Input() color: string = '';
  @Input() customColor: string = '';
  @Input() outline: boolean = false;
  @Input() close: boolean = false;

  @Output() onClose = new EventEmitter();

  constructor() { }

  ngOnInit() { }

  delete(event: any, alert: any) {
    event.preventDefault();

    alert.remove();
    this.onClose.emit(event);
  }
}
