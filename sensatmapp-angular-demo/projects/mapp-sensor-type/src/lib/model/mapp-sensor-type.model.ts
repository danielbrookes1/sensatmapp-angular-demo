import { UUID } from "angular2-uuid";

export interface IMappSensorType {
  id: string;
  name: string;
}

export class MappSensorType implements IMappSensorType {
  id: string;
  name: string;

  constructor(mappSensorName?: string) {
    this.id = UUID.UUID();
    if (mappSensorName) {
      this.name = mappSensorName;
    }
  }
}
