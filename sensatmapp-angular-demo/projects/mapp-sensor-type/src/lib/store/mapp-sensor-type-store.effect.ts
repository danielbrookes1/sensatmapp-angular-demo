import { Type } from '@angular/core';
import { MappSensorTypeListEffects } from './mapp-sensor-type-list/mapp-sensor-type-list.effect';

export const MappSensorTypeStoreEffects: Type<any>[] = [
  MappSensorTypeListEffects
];
