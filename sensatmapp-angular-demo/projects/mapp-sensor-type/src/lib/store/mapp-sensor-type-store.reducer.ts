import { ActionReducerMap } from '@ngrx/store';
import { IMappSensorTypeStoreState } from './mapp-sensor-type-store.state';
import { mappSensorTypeListReducer } from './mapp-sensor-type-list/mapp-sensor-type-list.reducer';

export const MappSensorTypeStoreReducers: ActionReducerMap<IMappSensorTypeStoreState, any> = {
  mappSensorTypeList: mappSensorTypeListReducer
};
