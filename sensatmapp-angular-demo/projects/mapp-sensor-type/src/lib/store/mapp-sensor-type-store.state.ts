import { createFeatureSelector } from '@ngrx/store';
import { MappSensorTypeListState } from './mapp-sensor-type-list/mapp-sensor-type-list.state';

export interface IMappSensorTypeStoreState {
  mappSensorTypeList: MappSensorTypeListState;
}

export const getInitialMappSensorTypeStoreState = createFeatureSelector<IMappSensorTypeStoreState>(
  'mapp-sensor-type-store'
);
