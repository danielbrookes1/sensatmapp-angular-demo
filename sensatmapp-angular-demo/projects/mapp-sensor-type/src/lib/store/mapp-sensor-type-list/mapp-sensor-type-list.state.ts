import { EntityAdapter, createEntityAdapter, EntityState } from '@ngrx/entity';
import { IMappSensorType } from '../../model/mapp-sensor-type.model';

export const mappSensorTypeListAdapter: EntityAdapter<IMappSensorType> = createEntityAdapter<IMappSensorType>({
  selectId: model => model.id,
  sortComparer: (a: IMappSensorType, b: IMappSensorType): number =>
    a.name.toString().localeCompare(b.name.toString())
  });

export interface MappSensorTypeListState extends EntityState<IMappSensorType> {
  inserting: boolean;
  inserted: boolean;
}

export const initialMappSensorTypeListState: MappSensorTypeListState = mappSensorTypeListAdapter.getInitialState({
  inserting: false,
  inserted: false
});
