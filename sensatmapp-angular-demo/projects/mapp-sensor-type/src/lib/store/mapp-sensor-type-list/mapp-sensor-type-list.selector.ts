import { createFeatureSelector, createSelector } from '@ngrx/store';
import { selectAll } from './mapp-sensor-type-list.reducer';
import { IMappSensorTypeStoreState } from '../mapp-sensor-type-store.state';

export const getMappSensorTypeStoreState = createFeatureSelector<IMappSensorTypeStoreState>('mapp-sensor-type-store');
export const getMappSensorTypeListState = createSelector(getMappSensorTypeStoreState, state => state.mappSensorTypeList);
export const getMappSensorTypeListItems = createSelector(getMappSensorTypeListState, selectAll);
export const getMappSensorTypeListInserting = createSelector(getMappSensorTypeListState, state => state.inserting);
export const getMappSensorTypeListInserted = createSelector(getMappSensorTypeListState, state => state.inserted);
