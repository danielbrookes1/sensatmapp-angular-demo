import { MappSensorTypeListActions, MappSensorTypeListActionTypes } from './mapp-sensor-type-list.action';
import { MappSensorTypeListState, initialMappSensorTypeListState, mappSensorTypeListAdapter } from './mapp-sensor-type-list.state';

export function mappSensorTypeListReducer(
  state = initialMappSensorTypeListState,
  action: MappSensorTypeListActions
): MappSensorTypeListState {
  switch (action.type) {
    case MappSensorTypeListActionTypes.INSERT_MAPPSENSORTYPELIST: return { ...state, inserting: true, inserted: false };
    case MappSensorTypeListActionTypes.INSERT_MAPPSENSORTYPELIST_SUCCESS: return mappSensorTypeListAdapter.setAll(action.payload.mappSensorTypeList, { ...state, inserting: false, inserted: true });
    case MappSensorTypeListActionTypes.INSERT_MAPPSENSORTYPELIST_FAIL: return { ...state, inserting: false, inserted: false };
    
    default: return state;
  }
}

export const { selectAll, selectEntities, selectIds, selectTotal } = mappSensorTypeListAdapter.getSelectors();
