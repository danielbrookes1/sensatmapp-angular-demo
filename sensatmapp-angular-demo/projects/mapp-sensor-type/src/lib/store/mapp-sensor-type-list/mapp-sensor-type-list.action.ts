import { Action } from '@ngrx/store';
import { IMappSensorType } from '../../model/mapp-sensor-type.model';

//#region ActionTypes
export enum MappSensorTypeListActionTypes {
  INSERT_MAPPSENSORTYPELIST = '[mapp-sensor-type-store] Load MappSensorTypeList',
  INSERT_MAPPSENSORTYPELIST_SUCCESS = '[mapp-sensor-type-store] Load MappSensorTypeList Success',
  INSERT_MAPPSENSORTYPELIST_FAIL = '[mapp-sensor-type-store] Load MappSensorTypeList Fail'
}
//#endregion ActionTypes

//#region InsertMappSensorTypeList
export class InsertMappSensorTypeList implements Action {
  readonly type = MappSensorTypeListActionTypes.INSERT_MAPPSENSORTYPELIST;
  constructor(public payload: { mappSensorTypeList: IMappSensorType[] }) {}
}

export class InsertMappSensorTypeListSuccess implements Action {
  readonly type = MappSensorTypeListActionTypes.INSERT_MAPPSENSORTYPELIST_SUCCESS;
  constructor(public payload: { mappSensorTypeList: IMappSensorType[] }) {}
}

export class InsertMappSensorTypeListFail implements Action {
  readonly type = MappSensorTypeListActionTypes.INSERT_MAPPSENSORTYPELIST_FAIL;
  constructor(public payload: { mappSenorTypeList: IMappSensorType[], error: any }) {}
}
//#endregion InsertMappSensorTypeList

//#region Actions
export type MappSensorTypeListActions =
  InsertMappSensorTypeList
  | InsertMappSensorTypeListSuccess
  | InsertMappSensorTypeListFail;
//#endregion Actions
