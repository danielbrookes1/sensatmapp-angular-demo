import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { of, Observable } from 'rxjs';
import { map, switchMap, catchError, withLatestFrom, filter } from 'rxjs/operators';
import { Store, Action } from '@ngrx/store';
import { LoggerService } from 'projects/logger/src/public-api';

import {
  MappSensorTypeListActionTypes,
  InsertMappSensorTypeList,
  InsertMappSensorTypeListSuccess,
  InsertMappSensorTypeListFail
} from './mapp-sensor-type-list.action';
import { IMappSensorTypeStoreState } from '../mapp-sensor-type-store.state';
import { MappSensorType } from '../../model/mapp-sensor-type.model';
import { LoadMappReadingListSuccess, MappReadingListActionTypes } from 'projects/mapp-reading/src/lib/store/mapp-reading-store.action';

@Injectable()
export class MappSensorTypeListEffects {
  namespace: string = 'MappSensorTypeListEffects';

  constructor(
    private actions$: Actions,
    private store: Store<IMappSensorTypeStoreState>,
    private logger: LoggerService
  ) {}

  //#region InsertMappSensorTypeList
  insertMappSensorTypeList$ = createEffect(() => this.actions$.pipe(
    ofType<InsertMappSensorTypeList>(MappSensorTypeListActionTypes.INSERT_MAPPSENSORTYPELIST),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.insertMappSensorTypeList$`);
      return new InsertMappSensorTypeListSuccess({
        mappSensorTypeList: payload.mappSensorTypeList
      });
    })
  ));

  insertMappSensorTypeListSuccess$ = createEffect(() => this.actions$.pipe(
    ofType<InsertMappSensorTypeListSuccess>(MappSensorTypeListActionTypes.INSERT_MAPPSENSORTYPELIST_SUCCESS),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.insertMappSensorTypeListSuccess$`);
    })
  ), { dispatch: false });
  
  insertMappSensorTypeListFail$ = createEffect(() => this.actions$.pipe(
    ofType<InsertMappSensorTypeListFail>(MappSensorTypeListActionTypes.INSERT_MAPPSENSORTYPELIST_FAIL),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.insertMappSensorTypeListFail$`);
      throw new Error(`Failed to insert the sensor types: ${payload.error.error.message}`);
    })
  ), { dispatch: false });
  //#endregion InsertMappSensorTypeList

  //#region Third-party
  loadMappReadingListSuccess$ = createEffect(() => this.actions$.pipe(
    ofType<LoadMappReadingListSuccess>(MappReadingListActionTypes.LOAD_MAPPREADINGLIST_SUCCESS),
    map(action => action.payload),
    map(payload => ({
      ...payload,
      mappSensorTypeList: [...new Set(payload.mappReadingList.map(o => o.sensorType))].map(o => new MappSensorType(o))
    })),
    map(payload => {
      this.logger.info(`${this.namespace}.loadMappReadingListSuccess$ | SensorTypes: ${payload.mappSensorTypeList.length}`);
      return new InsertMappSensorTypeList({
        mappSensorTypeList: payload.mappSensorTypeList
      });
    })
  ));
  //#endregion Third-party
  
}
