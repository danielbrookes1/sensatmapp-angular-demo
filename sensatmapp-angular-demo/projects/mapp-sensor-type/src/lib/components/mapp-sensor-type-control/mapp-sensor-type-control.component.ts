import { Component, EventEmitter, OnInit, Input, Output, ViewChild, AfterViewInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { NgForm } from '@angular/forms';
import { LoggerService } from '../../../../../logger/src/public-api';
import { Store } from '@ngrx/store';
import { MatSelect } from '@angular/material/select';
import { IMappSensorTypeStoreState } from '../../store/mapp-sensor-type-store.state';
import { getMappSensorTypeListItems } from '../../store/mapp-sensor-type-store.selector';
import { IMappSensorType } from '../../model/mapp-sensor-type.model';

@Component({
  selector: 'cmp-mapp-sensor-type-control',
  templateUrl: './mapp-sensor-type-control.component.html',
  styleUrls: ['./mapp-sensor-type-control.component.scss'],
  providers: []
})

export class MappSensorTypeControlComponent implements OnInit, AfterViewInit {
  namespace: string = 'MappSensorTypeControl';

  @ViewChild('mappSensorType', { read: MatSelect }) control: MatSelect;
  @ViewChild('frmMappSensorTypeControl', { static: false }) form: NgForm;

  @Input()
  set mappSensorTypeName(mappSensorTypeName: string) {
    this._mappSensorTypeName = mappSensorTypeName;
  }

  get mappSensorTypeName() {
    return this._mappSensorTypeName;
  }

  @Input()
  disabled: boolean = false;

  @Input()
  autoSelectSingleOption: boolean = true;

  @Input()
  set reset(reset: boolean) {
    if (reset == true) {
      setTimeout(() => {
        this.onTryReset();
      }, 10);
    }
  }

  @Output()
  change: EventEmitter<string> = new EventEmitter<string>();

  private _mappSensorTypeName: string;
  mappSensorTypeList$: Observable<IMappSensorType[]>;
  
  constructor(
    private store: Store<IMappSensorTypeStoreState>,
    private logger: LoggerService
  ) {
    this.logger.info(`${this.namespace}.ctor`);
  }

  ngOnInit() {
    this.logger.info(`${this.namespace}.ngOnInit`);
    this.mappSensorTypeList$ = this.store.select(getMappSensorTypeListItems);
  }

  ngAfterViewInit() {
    if (this.autoSelectSingleOption && this.control.options.length === 1) {
      this.onMappSensorTypeChange(this.control.options.first.value);
    }
  }

  onTryReset() {
    this.logger.info(`${this.namespace}.onTryReset`);
    if (this.form) {
      this.form.resetForm();
    }
  }

  onMappSensorTypeChange(mappSensorTypeName: string) {
    this.logger.info(`${this.namespace}.onMappSensorTypeChange`);
    this.change.emit(mappSensorTypeName);
  }

  onMappSensorTypeClear(e: Event, mappSensorTypeName: string) {
    this.logger.info(`${this.namespace}.onMappSensorTypeClear`);
    e.preventDefault();
    e.stopPropagation();
    mappSensorTypeName = null;
    this.onMappSensorTypeChange(mappSensorTypeName);
  }

}
