import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { MappSensorTypeStoreReducers } from '../../store/mapp-sensor-type-store.reducer';
import { MappSensorTypeControlComponent } from './mapp-sensor-type-control.component';
import { MappSensorTypeStoreEffects } from '../../store/mapp-sensor-type-store.effect';

describe('MappSensorTypeControlComponent', () => {
  let component: MappSensorTypeControlComponent;
  let fixture: ComponentFixture<MappSensorTypeControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        StoreModule.forFeature('mapp-sensor-type-store', MappSensorTypeStoreReducers),
        EffectsModule.forFeature(MappSensorTypeStoreEffects)
      ],
      declarations: [
        MappSensorTypeControlComponent
      ],
      providers: [

      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MappSensorTypeControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
