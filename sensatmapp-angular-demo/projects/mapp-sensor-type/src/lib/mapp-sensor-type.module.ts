import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { CustomMaterialModule, UiComponentsModule } from 'projects/ui-components/src/public-api';
import { MappSensorTypeControlComponent } from './components/mapp-sensor-type-control/mapp-sensor-type-control.component';
import { MappSensorTypeStoreEffects } from './store/mapp-sensor-type-store.effect';
import { MappSensorTypeStoreReducers } from './store/mapp-sensor-type-store.reducer';

@NgModule({
  declarations: [
    MappSensorTypeControlComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    UiComponentsModule,
    CustomMaterialModule,
    StoreModule.forFeature('mapp-sensor-type-store', MappSensorTypeStoreReducers),
    EffectsModule.forFeature(MappSensorTypeStoreEffects)
  ],
  exports: [
    MappSensorTypeControlComponent
  ]
})
export class MappSensorTypeModule { }
