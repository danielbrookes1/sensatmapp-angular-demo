import { Component, EventEmitter, OnInit, Input, Output, ViewChild, AfterViewInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { NgForm } from '@angular/forms';
import { LoggerService } from '../../../../../logger/src/public-api';
import { Store } from '@ngrx/store';
import { MatSelect } from '@angular/material/select';
import { IMappDataTypeStoreState } from '../../store/mapp-data-type-store.state';
import { getMappDataTypeListItems } from '../../store/mapp-data-type-store.selector';
import { IMappDataType } from '../../model/mapp-data-type.model';

@Component({
  selector: 'cmp-mapp-data-type-control',
  templateUrl: './mapp-data-type-control.component.html',
  styleUrls: ['./mapp-data-type-control.component.scss'],
  providers: []
})

export class MappDataTypeControlComponent implements OnInit, AfterViewInit {
  namespace: string = 'MappDataTypeControl';

  @ViewChild('mappDataType', { read: MatSelect }) control: MatSelect;
  @ViewChild('frmMappDataTypeControl', { static: false }) form: NgForm;

  @Input()
  set mappDataTypeName(mappDataTypeName: string) {
    this._mappDataTypeName = mappDataTypeName;
  }

  get mappDataTypeName() {
    return this._mappDataTypeName;
  }

  @Input()
  disabled: boolean = false;

  @Input()
  autoSelectSingleOption: boolean = true;

  @Input()
  set reset(reset: boolean) {
    if (reset == true) {
      setTimeout(() => {
        this.onTryReset();
      }, 10);
    }
  }

  @Output()
  change: EventEmitter<string> = new EventEmitter<string>();

  private _mappDataTypeName: string;
  mappDataTypeList$: Observable<IMappDataType[]>;
  
  constructor(
    private store: Store<IMappDataTypeStoreState>,
    private logger: LoggerService
  ) {
    this.logger.info(`${this.namespace}.ctor`);
  }

  ngOnInit() {
    this.logger.info(`${this.namespace}.ngOnInit`);
    this.mappDataTypeList$ = this.store.select(getMappDataTypeListItems);
  }

  ngAfterViewInit() {
    if (this.autoSelectSingleOption && this.control.options.length === 1) {
      this.onMappDataTypeChange(this.control.options.first.value);
    }
  }

  onTryReset() {
    this.logger.info(`${this.namespace}.onTryReset`);
    if (this.form) {
      this.form.resetForm();
    }
  }

  onMappDataTypeChange(mappDataTypeName: string) {
    this.logger.info(`${this.namespace}.onMappDataTypeChange`);
    this.change.emit(mappDataTypeName);
  }

  onMappDataTypeClear(e: Event, mappDataTypeName: string) {
    this.logger.info(`${this.namespace}.onMappDataTypeClear`);
    e.preventDefault();
    e.stopPropagation();
    mappDataTypeName = null;
    this.onMappDataTypeChange(mappDataTypeName);
  }

}
