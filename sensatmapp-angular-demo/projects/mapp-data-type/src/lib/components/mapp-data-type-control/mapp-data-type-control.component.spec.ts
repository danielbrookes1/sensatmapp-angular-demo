import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { MappDataTypeStoreReducers } from '../../store/mapp-data-type-store.reducer';
import { MappDataTypeControlComponent } from './mapp-data-type-control.component';
import { MappDataTypeStoreEffects } from '../../store/mapp-data-type-store.effect';

describe('MappDataTypeControlComponent', () => {
  let component: MappDataTypeControlComponent;
  let fixture: ComponentFixture<MappDataTypeControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        StoreModule.forFeature('mapp-data-type-store', MappDataTypeStoreReducers),
        EffectsModule.forFeature(MappDataTypeStoreEffects)
      ],
      declarations: [
        MappDataTypeControlComponent
      ],
      providers: [

      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MappDataTypeControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
