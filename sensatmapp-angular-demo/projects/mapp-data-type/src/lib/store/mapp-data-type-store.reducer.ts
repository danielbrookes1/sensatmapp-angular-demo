import { ActionReducerMap } from '@ngrx/store';
import { IMappDataTypeStoreState } from './mapp-data-type-store.state';
import { mappDataTypeListReducer } from './mapp-data-type-list/mapp-data-type-list.reducer';

export const MappDataTypeStoreReducers: ActionReducerMap<IMappDataTypeStoreState, any> = {
  mappDataTypeList: mappDataTypeListReducer
};
