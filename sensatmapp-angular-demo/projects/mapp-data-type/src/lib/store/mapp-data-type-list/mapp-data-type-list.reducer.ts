import { MappDataTypeListActions, MappDataTypeListActionTypes } from './mapp-data-type-list.action';
import { MappDataTypeListState, initialMappDataTypeListState, mappDataTypeListAdapter } from './mapp-data-type-list.state';

export function mappDataTypeListReducer(
  state = initialMappDataTypeListState,
  action: MappDataTypeListActions
): MappDataTypeListState {
  switch (action.type) {
    case MappDataTypeListActionTypes.INSERT_MAPPDATATYPELIST: return { ...state, inserting: true, inserted: false };
    case MappDataTypeListActionTypes.INSERT_MAPPDATATYPELIST_SUCCESS: return mappDataTypeListAdapter.setAll(action.payload.mappDataTypeList, { ...state, inserting: false, inserted: true });
    case MappDataTypeListActionTypes.INSERT_MAPPDATATYPELIST_FAIL: return { ...state, inserting: false, inserted: false };
    
    default: return state;
  }
}

export const { selectAll, selectEntities, selectIds, selectTotal } = mappDataTypeListAdapter.getSelectors();
