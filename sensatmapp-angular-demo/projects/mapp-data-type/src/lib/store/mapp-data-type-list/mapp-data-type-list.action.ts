import { Action } from '@ngrx/store';
import { IMappDataType } from '../../model/mapp-data-type.model';

//#region ActionTypes
export enum MappDataTypeListActionTypes {
  INSERT_MAPPDATATYPELIST = '[mapp-data-type-store] Load MappDataTypeList',
  INSERT_MAPPDATATYPELIST_SUCCESS = '[mapp-data-type-store] Load MappDataTypeList Success',
  INSERT_MAPPDATATYPELIST_FAIL = '[mapp-data-type-store] Load MappDataTypeList Fail'
}
//#endregion ActionTypes

//#region InsertMappDataTypeList
export class InsertMappDataTypeList implements Action {
  readonly type = MappDataTypeListActionTypes.INSERT_MAPPDATATYPELIST;
  constructor(public payload: { mappDataTypeList: IMappDataType[] }) {}
}

export class InsertMappDataTypeListSuccess implements Action {
  readonly type = MappDataTypeListActionTypes.INSERT_MAPPDATATYPELIST_SUCCESS;
  constructor(public payload: { mappDataTypeList: IMappDataType[] }) {}
}

export class InsertMappDataTypeListFail implements Action {
  readonly type = MappDataTypeListActionTypes.INSERT_MAPPDATATYPELIST_FAIL;
  constructor(public payload: { mappSenorTypeList: IMappDataType[], error: any }) {}
}
//#endregion InsertMappDataTypeList

//#region Actions
export type MappDataTypeListActions =
  InsertMappDataTypeList
  | InsertMappDataTypeListSuccess
  | InsertMappDataTypeListFail;
//#endregion Actions
