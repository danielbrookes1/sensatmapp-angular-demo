import { createFeatureSelector, createSelector } from '@ngrx/store';
import { selectAll } from './mapp-data-type-list.reducer';
import { IMappDataTypeStoreState } from '../mapp-data-type-store.state';

export const getMappDataTypeStoreState = createFeatureSelector<IMappDataTypeStoreState>('mapp-data-type-store');
export const getMappDataTypeListState = createSelector(getMappDataTypeStoreState, state => state.mappDataTypeList);
export const getMappDataTypeListItems = createSelector(getMappDataTypeListState, selectAll);
export const getMappDataTypeListInserting = createSelector(getMappDataTypeListState, state => state.inserting);
export const getMappDataTypeListInserted = createSelector(getMappDataTypeListState, state => state.inserted);
