import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { of, Observable } from 'rxjs';
import { map, switchMap, catchError, withLatestFrom, filter } from 'rxjs/operators';
import { Store, Action } from '@ngrx/store';
import { LoggerService } from 'projects/logger/src/public-api';

import {
  MappDataTypeListActionTypes,
  InsertMappDataTypeList,
  InsertMappDataTypeListSuccess,
  InsertMappDataTypeListFail
} from './mapp-data-type-list.action';
import { IMappDataTypeStoreState } from '../mapp-data-type-store.state';
import { MappDataType } from '../../model/mapp-data-type.model';
import { LoadMappReadingListSuccess, MappReadingListActionTypes } from 'projects/mapp-reading/src/lib/store/mapp-reading-store.action';

@Injectable()
export class MappDataTypeListEffects {
  namespace: string = 'MappDataTypeListEffects';

  constructor(
    private actions$: Actions,
    private store: Store<IMappDataTypeStoreState>,
    private logger: LoggerService
  ) {}

  //#region InsertMappDataTypeList
  insertMappDataTypeList$ = createEffect(() => this.actions$.pipe(
    ofType<InsertMappDataTypeList>(MappDataTypeListActionTypes.INSERT_MAPPDATATYPELIST),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.insertMappDataTypeList$`);
      return new InsertMappDataTypeListSuccess({
        mappDataTypeList: payload.mappDataTypeList
      });
    })
  ));

  insertMappDataTypeListSuccess$ = createEffect(() => this.actions$.pipe(
    ofType<InsertMappDataTypeListSuccess>(MappDataTypeListActionTypes.INSERT_MAPPDATATYPELIST_SUCCESS),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.insertMappDataTypeListSuccess$`);
    })
  ), { dispatch: false });
  
  insertMappDataTypeListFail$ = createEffect(() => this.actions$.pipe(
    ofType<InsertMappDataTypeListFail>(MappDataTypeListActionTypes.INSERT_MAPPDATATYPELIST_FAIL),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.insertMappDataTypeListFail$`);
      throw new Error(`Failed to insert the sensor types: ${payload.error.error.message}`);
    })
  ), { dispatch: false });
  //#endregion InsertMappDataTypeList

  //#region Third-party
  loadMappReadingListSuccess$ = createEffect(() => this.actions$.pipe(
    ofType<LoadMappReadingListSuccess>(MappReadingListActionTypes.LOAD_MAPPREADINGLIST_SUCCESS),
    map(action => action.payload),
    map(payload => ({
      ...payload,
      mappDataTypeList: [...new Set(payload.mappReadingList.map(o => o.dataType))].map(o => new MappDataType(o))
    })),
    map(payload => {
      this.logger.info(`${this.namespace}.loadMappReadingListSuccess$ | DataTypes: ${payload.mappDataTypeList.length}`);
      return new InsertMappDataTypeList({
        mappDataTypeList: payload.mappDataTypeList
      });
    })
  ));
  //#endregion Third-party
  
}
