import { EntityAdapter, createEntityAdapter, EntityState } from '@ngrx/entity';
import { IMappDataType } from '../../model/mapp-data-type.model';

export const mappDataTypeListAdapter: EntityAdapter<IMappDataType> = createEntityAdapter<IMappDataType>({
  selectId: model => model.id,
  sortComparer: (a: IMappDataType, b: IMappDataType): number =>
    a.name.toString().localeCompare(b.name.toString())
  });

export interface MappDataTypeListState extends EntityState<IMappDataType> {
  inserting: boolean;
  inserted: boolean;
}

export const initialMappDataTypeListState: MappDataTypeListState = mappDataTypeListAdapter.getInitialState({
  inserting: false,
  inserted: false
});
