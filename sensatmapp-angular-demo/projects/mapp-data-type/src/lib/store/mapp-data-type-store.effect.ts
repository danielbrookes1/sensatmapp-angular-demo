import { Type } from '@angular/core';
import { MappDataTypeListEffects } from './mapp-data-type-list/mapp-data-type-list.effect';

export const MappDataTypeStoreEffects: Type<any>[] = [
  MappDataTypeListEffects
];
