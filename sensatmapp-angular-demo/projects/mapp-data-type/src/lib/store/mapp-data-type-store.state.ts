import { createFeatureSelector } from '@ngrx/store';
import { MappDataTypeListState } from './mapp-data-type-list/mapp-data-type-list.state';

export interface IMappDataTypeStoreState {
  mappDataTypeList: MappDataTypeListState;
}

export const getInitialMappDataTypeStoreState = createFeatureSelector<IMappDataTypeStoreState>(
  'mapp-data-type-store'
);
