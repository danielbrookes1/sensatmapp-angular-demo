import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { CustomMaterialModule, UiComponentsModule } from 'projects/ui-components/src/public-api';
import { MappDataTypeControlComponent } from './components/mapp-data-type-control/mapp-data-type-control.component';
import { MappDataTypeStoreEffects } from './store/mapp-data-type-store.effect';
import { MappDataTypeStoreReducers } from './store/mapp-data-type-store.reducer';

@NgModule({
  declarations: [
    MappDataTypeControlComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    UiComponentsModule,
    CustomMaterialModule,
    StoreModule.forFeature('mapp-data-type-store', MappDataTypeStoreReducers),
    EffectsModule.forFeature(MappDataTypeStoreEffects)
  ],
  exports: [
    MappDataTypeControlComponent
  ]
})
export class MappDataTypeModule { }
