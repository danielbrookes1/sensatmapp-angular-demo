import { UUID } from "angular2-uuid";

export interface IMappDataType {
  id: string;
  name: string;
}

export class MappDataType implements IMappDataType {
  id: string;
  name: string;

  constructor(mappDataTypeName?: string) {
    this.id = UUID.UUID();
    if (mappDataTypeName) {
      this.name = mappDataTypeName;
    }
  }
}
