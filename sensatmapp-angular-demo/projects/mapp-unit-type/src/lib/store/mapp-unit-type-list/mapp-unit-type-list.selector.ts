import { createFeatureSelector, createSelector } from '@ngrx/store';
import { selectAll } from './mapp-unit-type-list.reducer';
import { IMappUnitTypeStoreState } from '../mapp-unit-type-store.state';

export const getMappUnitTypeStoreState = createFeatureSelector<IMappUnitTypeStoreState>('mapp-unit-type-store');
export const getMappUnitTypeListState = createSelector(getMappUnitTypeStoreState, state => state.mappUnitTypeList);
export const getMappUnitTypeListItems = createSelector(getMappUnitTypeListState, selectAll);
export const getMappUnitTypeListInserting = createSelector(getMappUnitTypeListState, state => state.inserting);
export const getMappUnitTypeListInserted = createSelector(getMappUnitTypeListState, state => state.inserted);
