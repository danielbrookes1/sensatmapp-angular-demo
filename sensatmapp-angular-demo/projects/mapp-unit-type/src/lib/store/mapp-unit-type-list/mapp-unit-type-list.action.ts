import { Action } from '@ngrx/store';
import { IMappUnitType } from '../../model/mapp-unit-type.model';

//#region ActionTypes
export enum MappUnitTypeListActionTypes {
  INSERT_MAPPUNITTYPELIST = '[mapp-unit-type-store] Load MappUnitTypeList',
  INSERT_MAPPUNITTYPELIST_SUCCESS = '[mapp-unit-type-store] Load MappUnitTypeList Success',
  INSERT_MAPPUNITTYPELIST_FAIL = '[mapp-unit-type-store] Load MappUnitTypeList Fail'
}
//#endregion ActionTypes

//#region InsertMappUnitTypeList
export class InsertMappUnitTypeList implements Action {
  readonly type = MappUnitTypeListActionTypes.INSERT_MAPPUNITTYPELIST;
  constructor(public payload: { mappUnitTypeList: IMappUnitType[] }) {}
}

export class InsertMappUnitTypeListSuccess implements Action {
  readonly type = MappUnitTypeListActionTypes.INSERT_MAPPUNITTYPELIST_SUCCESS;
  constructor(public payload: { mappUnitTypeList: IMappUnitType[] }) {}
}

export class InsertMappUnitTypeListFail implements Action {
  readonly type = MappUnitTypeListActionTypes.INSERT_MAPPUNITTYPELIST_FAIL;
  constructor(public payload: { mappSenorTypeList: IMappUnitType[], error: any }) {}
}
//#endregion InsertMappUnitTypeList

//#region Actions
export type MappUnitTypeListActions =
  InsertMappUnitTypeList
  | InsertMappUnitTypeListSuccess
  | InsertMappUnitTypeListFail;
//#endregion Actions
