import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { of, Observable } from 'rxjs';
import { map, switchMap, catchError, withLatestFrom, filter } from 'rxjs/operators';
import { Store, Action } from '@ngrx/store';
import { LoggerService } from 'projects/logger/src/public-api';

import {
  MappUnitTypeListActionTypes,
  InsertMappUnitTypeList,
  InsertMappUnitTypeListSuccess,
  InsertMappUnitTypeListFail
} from './mapp-unit-type-list.action';
import { IMappUnitTypeStoreState } from '../mapp-unit-type-store.state';
import { MappUnitType } from '../../model/mapp-unit-type.model';
import { LoadMappReadingListSuccess, MappReadingListActionTypes } from 'projects/mapp-reading/src/lib/store/mapp-reading-store.action';

@Injectable()
export class MappUnitTypeListEffects {
  namespace: string = 'MappUnitTypeListEffects';

  constructor(
    private actions$: Actions,
    private store: Store<IMappUnitTypeStoreState>,
    private logger: LoggerService
  ) {}

  //#region InsertMappUnitTypeList
  insertMappUnitTypeList$ = createEffect(() => this.actions$.pipe(
    ofType<InsertMappUnitTypeList>(MappUnitTypeListActionTypes.INSERT_MAPPUNITTYPELIST),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.insertMappUnitTypeList$`);
      return new InsertMappUnitTypeListSuccess({
        mappUnitTypeList: payload.mappUnitTypeList
      });
    })
  ));

  insertMappUnitTypeListSuccess$ = createEffect(() => this.actions$.pipe(
    ofType<InsertMappUnitTypeListSuccess>(MappUnitTypeListActionTypes.INSERT_MAPPUNITTYPELIST_SUCCESS),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.insertMappUnitTypeListSuccess$`);
    })
  ), { dispatch: false });
  
  insertMappUnitTypeListFail$ = createEffect(() => this.actions$.pipe(
    ofType<InsertMappUnitTypeListFail>(MappUnitTypeListActionTypes.INSERT_MAPPUNITTYPELIST_FAIL),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.insertMappUnitTypeListFail$`);
      throw new Error(`Failed to insert the unit types: ${payload.error.error.message}`);
    })
  ), { dispatch: false });
  //#endregion InsertMappUnitTypeList

  //#region Third-party
  loadMappReadingListSuccess$ = createEffect(() => this.actions$.pipe(
    ofType<LoadMappReadingListSuccess>(MappReadingListActionTypes.LOAD_MAPPREADINGLIST_SUCCESS),
    map(action => action.payload),
    map(payload => ({
      ...payload,
      mappUnitTypeList: [...new Set(payload.mappReadingList.map(o => o.unitType))].map(o => new MappUnitType(o))
    })),
    map(payload => {
      this.logger.info(`${this.namespace}.loadMappReadingListSuccess$ | UnitTypes: ${payload.mappUnitTypeList.length}`);
      return new InsertMappUnitTypeList({
        mappUnitTypeList: payload.mappUnitTypeList
      });
    })
  ));
  //#endregion Third-party
  
}
