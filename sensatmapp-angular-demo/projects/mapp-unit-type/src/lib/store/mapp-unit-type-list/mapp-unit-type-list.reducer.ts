import { MappUnitTypeListActions, MappUnitTypeListActionTypes } from './mapp-unit-type-list.action';
import { MappUnitTypeListState, initialMappUnitTypeListState, mappUnitTypeListAdapter } from './mapp-unit-type-list.state';

export function mappUnitTypeListReducer(
  state = initialMappUnitTypeListState,
  action: MappUnitTypeListActions
): MappUnitTypeListState {
  switch (action.type) {
    case MappUnitTypeListActionTypes.INSERT_MAPPUNITTYPELIST: return { ...state, inserting: true, inserted: false };
    case MappUnitTypeListActionTypes.INSERT_MAPPUNITTYPELIST_SUCCESS: return mappUnitTypeListAdapter.setAll(action.payload.mappUnitTypeList, { ...state, inserting: false, inserted: true });
    case MappUnitTypeListActionTypes.INSERT_MAPPUNITTYPELIST_FAIL: return { ...state, inserting: false, inserted: false };
    
    default: return state;
  }
}

export const { selectAll, selectEntities, selectIds, selectTotal } = mappUnitTypeListAdapter.getSelectors();
