import { EntityAdapter, createEntityAdapter, EntityState } from '@ngrx/entity';
import { IMappUnitType } from '../../model/mapp-unit-type.model';

export const mappUnitTypeListAdapter: EntityAdapter<IMappUnitType> = createEntityAdapter<IMappUnitType>({
  selectId: model => model.id,
  sortComparer: (a: IMappUnitType, b: IMappUnitType): number =>
    a.name.toString().localeCompare(b.name.toString())
  });

export interface MappUnitTypeListState extends EntityState<IMappUnitType> {
  inserting: boolean;
  inserted: boolean;
}

export const initialMappUnitTypeListState: MappUnitTypeListState = mappUnitTypeListAdapter.getInitialState({
  inserting: false,
  inserted: false
});
