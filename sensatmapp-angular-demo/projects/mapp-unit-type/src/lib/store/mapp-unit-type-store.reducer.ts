import { ActionReducerMap } from '@ngrx/store';
import { IMappUnitTypeStoreState } from './mapp-unit-type-store.state';
import { mappUnitTypeListReducer } from './mapp-unit-type-list/mapp-unit-type-list.reducer';

export const MappUnitTypeStoreReducers: ActionReducerMap<IMappUnitTypeStoreState, any> = {
  mappUnitTypeList: mappUnitTypeListReducer
};
