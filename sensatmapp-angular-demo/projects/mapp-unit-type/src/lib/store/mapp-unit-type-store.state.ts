import { createFeatureSelector } from '@ngrx/store';
import { MappUnitTypeListState } from './mapp-unit-type-list/mapp-unit-type-list.state';

export interface IMappUnitTypeStoreState {
  mappUnitTypeList: MappUnitTypeListState;
}

export const getInitialMappUnitTypeStoreState = createFeatureSelector<IMappUnitTypeStoreState>(
  'mapp-unit-type-store'
);
