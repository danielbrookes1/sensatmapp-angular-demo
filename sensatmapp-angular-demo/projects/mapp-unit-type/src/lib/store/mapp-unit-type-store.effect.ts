import { Type } from '@angular/core';
import { MappUnitTypeListEffects } from './mapp-unit-type-list/mapp-unit-type-list.effect';

export const MappUnitTypeStoreEffects: Type<any>[] = [
  MappUnitTypeListEffects
];
