import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { CustomMaterialModule, UiComponentsModule } from 'projects/ui-components/src/public-api';
import { MappUnitTypeControlComponent } from './components/mapp-unit-type-control/mapp-unit-type-control.component';
import { MappUnitTypeStoreEffects } from './store/mapp-unit-type-store.effect';
import { MappUnitTypeStoreReducers } from './store/mapp-unit-type-store.reducer';

@NgModule({
  declarations: [
    MappUnitTypeControlComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    UiComponentsModule,
    CustomMaterialModule,
    StoreModule.forFeature('mapp-unit-type-store', MappUnitTypeStoreReducers),
    EffectsModule.forFeature(MappUnitTypeStoreEffects)
  ],
  exports: [
    MappUnitTypeControlComponent
  ]
})
export class MappUnitTypeModule { }
