import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { MappUnitTypeStoreReducers } from '../../store/mapp-unit-type-store.reducer';
import { MappUnitTypeControlComponent } from './mapp-unit-type-control.component';
import { MappUnitTypeStoreEffects } from '../../store/mapp-unit-type-store.effect';

describe('MappUnitTypeControlComponent', () => {
  let component: MappUnitTypeControlComponent;
  let fixture: ComponentFixture<MappUnitTypeControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        StoreModule.forFeature('mapp-sensor-type-store', MappUnitTypeStoreReducers),
        EffectsModule.forFeature(MappUnitTypeStoreEffects)
      ],
      declarations: [
        MappUnitTypeControlComponent
      ],
      providers: [

      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MappUnitTypeControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
