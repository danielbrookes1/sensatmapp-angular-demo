import { Component, EventEmitter, OnInit, Input, Output, ViewChild, AfterViewInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { NgForm } from '@angular/forms';
import { LoggerService } from '../../../../../logger/src/public-api';
import { Store } from '@ngrx/store';
import { MatSelect } from '@angular/material/select';
import { IMappUnitTypeStoreState } from '../../store/mapp-unit-type-store.state';
import { getMappUnitTypeListItems } from '../../store/mapp-unit-type-store.selector';
import { IMappUnitType } from '../../model/mapp-unit-type.model';

@Component({
  selector: 'cmp-mapp-unit-type-control',
  templateUrl: './mapp-unit-type-control.component.html',
  styleUrls: ['./mapp-unit-type-control.component.scss'],
  providers: []
})

export class MappUnitTypeControlComponent implements OnInit, AfterViewInit {
  namespace: string = 'MappUnitTypeControl';

  @ViewChild('mappUnitType', { read: MatSelect }) control: MatSelect;
  @ViewChild('frmMappUnitTypeControl', { static: false }) form: NgForm;

  @Input()
  set mappUnitTypeName(mappUnitTypeName: string) {
    this._mappUnitTypeName = mappUnitTypeName;
  }

  get mappUnitTypeName() {
    return this._mappUnitTypeName;
  }

  @Input()
  disabled: boolean = false;

  @Input()
  autoSelectSingleOption: boolean = true;

  @Input()
  set reset(reset: boolean) {
    if (reset == true) {
      setTimeout(() => {
        this.onTryReset();
      }, 10);
    }
  }

  @Output()
  change: EventEmitter<string> = new EventEmitter<string>();

  private _mappUnitTypeName: string;
  mappUnitTypeList$: Observable<IMappUnitType[]>;
  
  constructor(
    private store: Store<IMappUnitTypeStoreState>,
    private logger: LoggerService
  ) {
    this.logger.info(`${this.namespace}.ctor`);
  }

  ngOnInit() {
    this.logger.info(`${this.namespace}.ngOnInit`);
    this.mappUnitTypeList$ = this.store.select(getMappUnitTypeListItems);
  }

  ngAfterViewInit() {
    if (this.autoSelectSingleOption && this.control.options.length === 1) {
      this.onMappUnitTypeChange(this.control.options.first.value);
    }
  }

  onTryReset() {
    this.logger.info(`${this.namespace}.onTryReset`);
    if (this.form) {
      this.form.resetForm();
    }
  }

  onMappUnitTypeChange(mappUnitTypeName: string) {
    this.logger.info(`${this.namespace}.onMappUnitTypeChange`);
    this.change.emit(mappUnitTypeName);
  }

  onMappUnitTypeClear(e: Event, mappUnitTypeName: string) {
    this.logger.info(`${this.namespace}.onMappUnitTypeClear`);
    e.preventDefault();
    e.stopPropagation();
    mappUnitTypeName = null;
    this.onMappUnitTypeChange(mappUnitTypeName);
  }

}
