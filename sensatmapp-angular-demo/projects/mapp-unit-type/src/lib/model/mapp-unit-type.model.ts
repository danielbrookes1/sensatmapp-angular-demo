import { UUID } from "angular2-uuid";

export interface IMappUnitType {
  id: string;
  name: string;
}

export class MappUnitType implements IMappUnitType {
  id: string;
  name: string;

  constructor(mappUnitName?: string) {
    this.id = UUID.UUID();
    if (mappUnitName) {
      this.name = mappUnitName;
    }
  }
}
