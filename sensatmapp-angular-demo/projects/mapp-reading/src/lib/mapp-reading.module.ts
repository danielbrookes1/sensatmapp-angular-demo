import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { MappDataTypeModule } from 'projects/mapp-data-type/src/public-api';
import { MappSensorTypeModule } from 'projects/mapp-sensor-type/src/public-api';
import { MappUnitTypeModule } from 'projects/mapp-unit-type/src/public-api';
import { CustomMaterialModule, UiComponentsModule } from 'projects/ui-components/src/public-api';
import { MappReadingItemComponent } from './component/mapp-reading-item/mapp-reading-item.component';
import { MappReadingListComponent } from './component/mapp-reading-list/mapp-reading-list.component';
import { MappReadingTableComponent } from './component/mapp-reading-table/mapp-reading-table.component';
import { MappReadingComponent } from './container/mapp-reading.component';
import { MappReadingPipeModule } from './pipe/mapp-reading-pipe.module';
import { MappReadingStoreEffects } from './store/mapp-reading-store.effect';
import { MappReadingStoreReducers } from './store/mapp-reading-store.reducer';

@NgModule({
  declarations: [
    MappReadingComponent,
    MappReadingListComponent,
    MappReadingItemComponent,
    MappReadingTableComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    UiComponentsModule,
    CustomMaterialModule,
    MappReadingPipeModule,
    StoreModule.forFeature('mapp-reading-store', MappReadingStoreReducers),
    EffectsModule.forFeature(MappReadingStoreEffects),
    MappSensorTypeModule,
    MappDataTypeModule,
    MappUnitTypeModule
  ],
  exports: [
    MappReadingPipeModule,
    MappReadingComponent
  ]
})
export class MappReadingModule { }
