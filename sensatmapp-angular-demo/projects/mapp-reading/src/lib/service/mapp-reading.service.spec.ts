import { TestBed } from '@angular/core/testing';

import { MappReadingService } from './mapp-reading.service';

describe('MappReadingService', () => {
  let service: MappReadingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MappReadingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
