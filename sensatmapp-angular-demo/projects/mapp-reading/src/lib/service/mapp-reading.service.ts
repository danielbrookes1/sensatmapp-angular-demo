import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { IMappReadingResponse } from '../model/response/mapp-reading-response.model';
import { IMappReading, MappReading } from '../model/mapp-reading.model';
import { LoggerService } from 'projects/logger/src/public-api';

@Injectable({
  providedIn: 'root'
})

export class MappReadingService {
  private namespace: string = 'MappReadingService';
  private servicePath: string = 'assets/data';

  constructor(
    private http: HttpClient,
    private logger: LoggerService
    ) {

  }

  /**
   * @description Returns all available readings
   */
  public getAll(): Observable<IMappReading[]> {
    this.logger.info(`${this.namespace}.getAll`);
    const url: string = `${this.servicePath}/sensor_readings.json`;
    return this.http.get<IMappReadingResponse[]>(
        url
      ).pipe(map(r => r.map(o => new MappReading(o)))
    );
  }


}
