import { Component, OnInit, EventEmitter, Input, Output, ViewChild, AfterViewInit } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { LoggerService } from 'projects/logger/src/public-api';
import { IMappReading } from '../../model/mapp-reading.model';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'cmp-mapp-reading-table',
  templateUrl: './mapp-reading-table.component.html',
  styleUrls: ['./mapp-reading-table.component.scss']
})
export class MappReadingTableComponent implements OnInit, AfterViewInit {
  namespace: string = 'MappReadingTableComponent';

  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  @Input()
  mappReadingList: IMappReading[];

  private _isFiltered: boolean = false;
  @Input()
  set isFiltered(isFiltered: boolean) {
    if (this._isFiltered != isFiltered) {
      this._isFiltered = isFiltered;
      this.dataSource = new MatTableDataSource(this.mappReadingList);
    }
  };

  @Output()
  select: EventEmitter<IMappReading> = new EventEmitter<IMappReading>();

  dataSource: MatTableDataSource<IMappReading>;
  displayedColumns: string[] = [
    'readingTs',
    'containerId',
    'sensorType',
    'dataType',
    'rangeLower',
    'rangeUpper',
    'longitude',
    'latitude',
    'value',
    'unitType',
    'rowActions'
  ];

  constructor(
    private logger: LoggerService
  ) {
    this.logger.info(`${this.namespace}.ctor`);

  }

  ngOnInit() {
    this.logger.info(`${this.namespace}.ngOnInit`);
    this.dataSource = new MatTableDataSource(this.mappReadingList);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  onSelectClick(e: Event, mappReading: IMappReading) {
    this.logger.info(`${this.namespace}.onSelectClick | Reading: ${mappReading.id}`);
    e.preventDefault();
    e.stopPropagation();
    this.onSelect(mappReading);
  }

  onSelect(mappReading: IMappReading) {
    this.logger.info(`${this.namespace}.onSelect | Reading: ${mappReading.id}`);
    this.select.emit(mappReading);
  }

}
