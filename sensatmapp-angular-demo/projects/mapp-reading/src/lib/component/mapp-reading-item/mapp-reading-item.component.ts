import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UUID } from 'angular2-uuid';
import { LoggerService } from 'projects/logger/src/public-api';
import { IMappReading } from '../../model/mapp-reading.model';

@Component({
  selector: 'cmp-mapp-reading-item',
  templateUrl: './mapp-reading-item.component.html',
  styleUrls: ['./mapp-reading-item.component.scss']
})

export class MappReadingItemComponent implements OnInit {
  namespace: string = 'MappReadingItemComponent';
  
  @ViewChild('mappReadingItemForm') mappReadingItemForm: NgForm;
  
  @Input()
  set mappReading(mappReading: IMappReading) {
    this._mappReading = Object.assign({}, mappReading);
  };

  get mappReading(): IMappReading {
    return this._mappReading;
  }

  @Output()
  deselect: EventEmitter<void> = new EventEmitter<void>();

  @Output()
  create: EventEmitter<IMappReading> = new EventEmitter<IMappReading>();

  @Output()
  update: EventEmitter<IMappReading> = new EventEmitter<IMappReading>();

  @Output()
  remove: EventEmitter<IMappReading> = new EventEmitter<IMappReading>();

  _mappReading: IMappReading;
  isLocked: boolean = false;

  constructor(
    public snackBar: MatSnackBar,
    private logger: LoggerService
  ) {
    this.logger.info(`${this.namespace}.ctor`);
  }

  ngOnInit() {
    this.logger.info(`${this.namespace}.ngOnInit`);
  }

  onReset() {
    this.logger.info(`${this.namespace}.onReset`);
    if (this.mappReadingItemForm) {
      this.mappReadingItemForm.reset();
    }
  }

  onDeselectClick(e: Event) {
    this.logger.info(`${this.namespace}.onDeselectClick`);
    e.preventDefault();
    e.stopPropagation();
    this.deselect.emit();
    this.onReset();
  }

  onCreateClick(e: Event, mappReading: IMappReading) {
    this.logger.info(`${this.namespace}.onCreateClick | Reading: ${mappReading.id}`);
    e.preventDefault();
    e.stopPropagation();
    this.isLocked = true;
    this.create.emit(mappReading);
  }

  onUpdateClick(e: Event, mappReading: IMappReading) {
    this.logger.info(`${this.namespace}.onUpdateClick | Reading: ${mappReading.id}`);
    e.preventDefault();
    e.stopPropagation();
    this.isLocked = true;
    this.update.emit(mappReading);
    this.onReset();
  }

  onRemoveClick(e: Event, mappReading: IMappReading) {
    this.logger.info(`${this.namespace}.onRemoveClick | Reading: ${mappReading.id}`);
    e.preventDefault();
    e.stopPropagation();
    this.onRemoveConfirm(mappReading);
  }
  
  onRemoveConfirm(mappReading: IMappReading) {
    this.logger.info(`${this.namespace}.onRemoveConfirm | Reading: ${mappReading.id}`);
    const snackbar = this.snackBar.open(`Are you sure you want to delete this reading?`, 'Yes, delete it', {
      duration: 5000,
      panelClass: 'snackbar-warning'
    });
    snackbar.onAction().subscribe(() => {
      this.onRemove(mappReading);
    });
  }

  onRemove(mappReading: IMappReading) {
    this.logger.info(`${this.namespace}.onRemove | Reading: ${mappReading.id}`);
    this.remove.emit(mappReading);
  }

  onMappSensorTypeChange(mappReading: IMappReading, mappSensorTypeName: string) {
    this.logger.info(`${this.namespace}.onMappSensorTypeChange | Reading: ${mappReading.id}, SensorType: ${mappSensorTypeName}`);
    mappReading.sensorType = mappSensorTypeName;
  }

  onMappDataTypeChange(mappReading: IMappReading, mappDataTypeName: string) {
    this.logger.info(`${this.namespace}.onMappDataTypeChange | Reading: ${mappReading.id}, DataType: ${mappDataTypeName}`);
    mappReading.dataType = mappDataTypeName;
  }

  onMappUnitTypeChange(mappReading: IMappReading, mappUnitTypeName: string) {
    this.logger.info(`${this.namespace}.onMappUnitTypeChange | Reading: ${mappReading.id}, UnitType: ${mappUnitTypeName}`);
    mappReading.unitType = mappUnitTypeName;
  }

  onGenerateMappReadId(e: Event, mappReading: IMappReading) {
    this.logger.info(`${this.namespace}.onGenerateMappReadId | Reading: ${mappReading.id}`);
    e.preventDefault();
    e.stopPropagation();
    mappReading.readId = UUID.UUID();
  }

  onClearMappReadId(e: Event, mappReading: IMappReading) {
    this.logger.info(`${this.namespace}.onGenerateMappReadId | Reading: ${mappReading.id}`);
    e.preventDefault();
    e.stopPropagation();
    mappReading.readId = null;
  }

  onGenerateMappContainerId(e: Event, mappReading: IMappReading) {
    this.logger.info(`${this.namespace}.onGenerateMappContainerId | Reading: ${mappReading.id}`);
    e.preventDefault();
    e.stopPropagation();
    mappReading.containerId = UUID.UUID();
  }

  onClearMappContainerId(e: Event, mappReading: IMappReading) {
    this.logger.info(`${this.namespace}.onGenerateMappContainerId | Reading: ${mappReading.id}`);
    e.preventDefault();
    e.stopPropagation();
    mappReading.containerId = null;
  }

}
