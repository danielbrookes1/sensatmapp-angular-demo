import { Component, OnInit, EventEmitter, Input, Output, ViewChild, ElementRef } from '@angular/core';
import { LoggerService } from 'projects/logger/src/public-api';
import { IMappReading } from '../../model/mapp-reading.model';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'cmp-mapp-reading-list',
  templateUrl: './mapp-reading-list.component.html',
  styleUrls: ['./mapp-reading-list.component.scss']
})
export class MappReadingListComponent implements OnInit {
  namespace: string = 'MappReadingListComponent';

  @Input()
  mappReadingList: IMappReading[];

  @Input()
  mappReadingListRefreshed: Date;

  @Output()
  select: EventEmitter<IMappReading> = new EventEmitter<IMappReading>();

  @Output()
  addNew: EventEmitter<void> = new EventEmitter<void>();

  @Output()
  refresh: EventEmitter<void> = new EventEmitter<void>();

  dataSource: MatTableDataSource<IMappReading>;
  displayedColumns: string[] = [
    'readingTs',
    'containerId',
    'sensorType',
    'dataType',
    'rangeLower',
    'rangeUpper',
    'longitude',
    'latitude',
    'value',
    'unitType',
    'rowActions'
  ];

  private _searchControl: ElementRef;
  @ViewChild('mappReadingSearch', { read: ElementRef, static: false })
  set searchControl(ctrl: ElementRef) {
    if (ctrl) {
      this._searchControl = ctrl;
    }
  };

  get searchControl() {
    return this._searchControl;
  }
  isSearchVisible: boolean = false;
  isFiltered: boolean = false;
  searchText: string = '';

  constructor(
    private logger: LoggerService
  ) {
    this.logger.info(`${this.namespace}.ctor`);

  }

  ngOnInit() {
    this.logger.info(`${this.namespace}.ngOnInit`);
  }

  onSearchShowClick(e: Event) {
    this.logger.info(`${this.namespace}.onSearchShowClick`);
    e.preventDefault();
    e.stopPropagation();
    this.onSearchShow();
  }

  onSearchShow() {
    this.logger.info(`${this.namespace}.onSearchShow`);
    this.isSearchVisible = true;
    setTimeout(() => {
      if (this.searchControl) {
        this.searchControl.nativeElement.focus();
      }
    }, 10);
  }

  onSearchCancelClick(e: Event) {
    this.logger.info(`${this.namespace}.onSearchCancelClick`);
    e.preventDefault();
    e.stopPropagation();
    this.searchText = '';
    this.isFiltered = false;
    this.onSearchHide();
  }

  onSearchHide() {
    this.logger.info(`${this.namespace}.onSearchHide`);
    this.isSearchVisible = false;
  }

  onSearchKeyUp(e: KeyboardEvent) {
    if (e.key === 'Enter') {
      this.isFiltered = true;
      this.onSearchHide();
    }
    if (e.key === "Escape") {
      this.onSearchCancelClick(e);
    }
  }

  onAddNewClick(e: Event) {
    this.logger.info(`${this.namespace}.onAddNewClick`);
    e.preventDefault();
    e.stopPropagation();
    this.addNew.emit();
  }

  onSelect(mappReading: IMappReading) {
    this.logger.info(`${this.namespace}.onSelect | Reading: ${mappReading.id}`);
    this.select.emit(mappReading);
  }

  onRefreshClick(e: Event) {
    this.logger.info(`${this.namespace}.onRefreshClick`);
    e.preventDefault();
    e.stopPropagation();
    this.refresh.emit();
  }

}
