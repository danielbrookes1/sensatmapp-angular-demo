import { createFeatureSelector, createSelector } from '@ngrx/store';
import { selectAll } from './mapp-reading-list.reducer';
import { IMappReadingStoreState } from '../mapp-reading-store.state';

export const getMappReadingStoreState = createFeatureSelector<IMappReadingStoreState>('mapp-reading-store');
export const getMappReadingListState = createSelector(getMappReadingStoreState, state => state.mappReadingList);
export const getMappReadingListItems = createSelector(getMappReadingListState, selectAll);
export const getMappReadingListLoading = createSelector(getMappReadingListState, state => state.loading || state.adding || state.creating || state.updating);
export const getMappReadingListLoaded = createSelector(getMappReadingListState, state => state.loaded && !(state.adding) && !(state.creating) && !(state.updating));
export const getMappReadingListRefreshed = createSelector(getMappReadingListState, state => state.refreshed);
export const getSelectedMappReadingId = createSelector(getMappReadingListState, state => state.selectedMappReadingId);
export const getSelectedMappReading = createSelector(getMappReadingListState, getMappReadingListItems, (state, items) => state.selectedMappReadingId ? items.find(o => o.id === state.selectedMappReadingId) : null);
