import { mappReadingListReducer, selectEntities, selectAll } from './mapp-reading-list.reducer';
import {
  SelectMappReading,
  SelectMappReadingSuccess,
  SelectMappReadingFail,
  DeselectMappReading,
  DeselectMappReadingSuccess,
  DeselectMappReadingFail,
  AddNewMappReading,
  AddNewMappReadingSuccess,
  AddNewMappReadingFail,
  CreateMappReading,
  CreateMappReadingSuccess,
  CreateMappReadingFail,
  UpdateMappReading,
  UpdateMappReadingSuccess,
  UpdateMappReadingFail
} from './mapp-reading-list.action';
import { initialMappReadingListState } from './mapp-reading-list.state';
import { MappReading, IMappReading } from '../../model/mapp-reading.model';
  // TODO: Make these dummy readings more realistic
  const mappReadingList: IMappReading[] = [
    Object.assign({}, new MappReading(), {
      readId: '56468045-7302-0a02-080e-ebf741819498',
      value: 10
    }),
    Object.assign({}, new MappReading(), {
      readId: '0d17a6bf-1642-53b1-a942-c01d3977293d',
      value: 100
    }),
    Object.assign({}, new MappReading(), {
      readId: 'd450c7a6-023c-3cfd-814d-776c0ba24d63',
      value: 1000
    })
  ];

describe('MappReadingListReducer', () => {

  describe('undefined action', () => {
    it('should return the default state', () => {
      const action = {} as any;
      const state = mappReadingListReducer(undefined, action);

      expect(state).toBe(initialMappReadingListState);
    });
  });

  describe('SELECT_MAPPREADING action', () => {
    it('should set selecting to true', () => {
      const payload = {
        mappReading: mappReadingList[0]
      };
      const action = new SelectMappReading(payload);
      const state = mappReadingListReducer(initialMappReadingListState, action);

      expect(state.selecting).toEqual(true);
      expect(state.selected).toEqual(false);
      expect(state.selectedMappReadingId).toEqual(null);
    });
  });

  describe('SELECT_MAPPREADING_SUCCESS action', () => {
    it('should set the selected process version', () => {
      const entities = {
        1: mappReadingList[0],
        2: mappReadingList[1],
      };
      const payload = {
        mappReading: mappReadingList[0]
      };
      const action = new SelectMappReadingSuccess(payload);
      const state = mappReadingListReducer(initialMappReadingListState, action);

      expect(state.selecting).toEqual(false);
      expect(state.selected).toEqual(true);
      expect(state.selectedMappReadingId).toEqual(mappReadingList[0].id);
    });
  });

  describe('SELECT_MAPPREADING_FAIL action', () => {
    it('should return the initial state', () => {
      const payload = {
        mappReading: mappReadingList[0],
        error: {
          error: {
            message: 'Failed'
          }
        }
      };
      const action = new SelectMappReadingFail(payload);
      const state = mappReadingListReducer(initialMappReadingListState, action);

      expect(state).toEqual(initialMappReadingListState);
    });

  });

  describe('DESELECT_MAPPREADING action', () => {
    it('should set deselecting to true', () => {
      const action = new DeselectMappReading();
      const state = mappReadingListReducer(initialMappReadingListState, action);

      expect(state.deselecting).toEqual(true);
      expect(state.deselected).toEqual(false);
    });
  });

  describe('DESELECT_MAPPREADING_SUCCESS action', () => {
    it('should set the selected process version to null', () => {
      const action = new DeselectMappReadingSuccess();
      const state = mappReadingListReducer(initialMappReadingListState, action);

      expect(state.deselecting).toEqual(false);
      expect(state.deselected).toEqual(true);
      expect(state.selectedMappReadingId).toEqual(null);
    });
  });

  describe('DESELECT_MAPPREADING_FAIL action', () => {
    it('should return the initial state', () => {
      const payload = {
        error: {
          error: {
            message: 'Failed'
          }
        }
      };
      const action = new DeselectMappReadingFail(payload);
      const state = mappReadingListReducer(initialMappReadingListState, action);

      expect(state).toEqual(initialMappReadingListState);
    });

  });

  describe('ADDNEW_MAPPREADING action', () => {
    it('should set adding to true', () => {
      const action = new AddNewMappReading();
      const state = mappReadingListReducer(initialMappReadingListState, action);

      expect(state.adding).toEqual(true);
      expect(state.added).toEqual(false);
    });
  });

  describe('ADDNEW_MAPPREADING_SUCCESS action', () => {
    it('should set added to true', () => {
      const payload = {
        mappReading: mappReadingList[0]
      };
      const action = new AddNewMappReadingSuccess(payload);
      const state = mappReadingListReducer(initialMappReadingListState, action);

      expect(state.adding).toEqual(false);
      expect(state.added).toEqual(true);
    });
  });

  describe('ADDNEW_MAPPREADING_FAIL action', () => {
    it('should set adding and added to false', () => {
      const payload = {
        error: { 
          error: {
            message: 'Failed'
          }
        }
      };
      const action = new AddNewMappReadingFail(payload);
      const state = mappReadingListReducer(initialMappReadingListState, action);

      expect(state.adding).toEqual(false);
      expect(state.added).toEqual(false);
    });
  });

  describe('CREATE_MAPPREADING action', () => {
    it('should set creating to true', () => {
      const payload =  {
        mappReading: mappReadingList[0]
      };
      const action = new CreateMappReading(payload);
      const state = mappReadingListReducer(initialMappReadingListState, action);

      expect(state.creating).toEqual(true);
      expect(state.created).toEqual(false);
    });
  });

  describe('CREATE_MAPPREADING_SUCCESS action', () => {
    it('should set created to true', () => {
      const payload = {
        mappReading: mappReadingList[0]
      };
      const action = new CreateMappReadingSuccess(payload);
      const state = mappReadingListReducer(initialMappReadingListState, action);

      expect(state.creating).toEqual(false);
      expect(state.created).toEqual(true);
    });
  });

  describe('CREATE_MAPPREADING_FAIL action', () => {
    it('should set creating and created to false', () => {
      const payload = {
        mappReading: mappReadingList[0],
        error: { 
          error: {
            message: 'Failed'
          }
        }
      };
      const action = new CreateMappReadingFail(payload);
      const state = mappReadingListReducer(initialMappReadingListState, action);

      expect(state.creating).toEqual(false);
      expect(state.created).toEqual(false);
    });
  });

  describe('UPDATE_MAPPREADING action', () => {
    it('should set updating to true', () => {
      const payload =  {
        mappReading: mappReadingList[0]
      };
      const action = new UpdateMappReading(payload);
      const state = mappReadingListReducer(initialMappReadingListState, action);

      expect(state.updating).toEqual(true);
      expect(state.updated).toEqual(false);
    });
  });

  describe('UPDATE_MAPPREADING_SUCCESS action', () => {
    it('should set updated to true', () => {
      const payload = {
        mappReading: mappReadingList[0]
      };
      const action = new UpdateMappReadingSuccess(payload);
      const state = mappReadingListReducer(initialMappReadingListState, action);

      expect(state.updating).toEqual(false);
      expect(state.updated).toEqual(true);
    });
  });

  describe('UPDATE_MAPPREADING_FAIL action', () => {
    it('should set updating and updated to false', () => {
      const payload = {
        mappReading: mappReadingList[0],
        error: { 
          error: {
            message: 'Failed'
          }
        }
      };
      const action = new UpdateMappReadingFail(payload);
      const state = mappReadingListReducer(initialMappReadingListState, action);

      expect(state.updating).toEqual(false);
      expect(state.updated).toEqual(false);
    });
  });

  describe('MappReadingListReducer Selectors', () => {
    describe('getMappReadingListEntities', () => {
      it('should return entities', () => {
        const entities = {
          1: mappReadingList[0],
          2: mappReadingList[1],
        };
        const previousState = { ...initialMappReadingListState, entities };
        const slice = selectEntities(previousState);

        expect(slice).toEqual(entities);
      });
    });

  })

});
