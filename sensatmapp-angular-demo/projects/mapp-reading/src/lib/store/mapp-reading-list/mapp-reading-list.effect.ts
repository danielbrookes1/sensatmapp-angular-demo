import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { of, Observable } from 'rxjs';
import { map, switchMap, catchError, withLatestFrom, filter } from 'rxjs/operators';
import { Store, Action } from '@ngrx/store';
import { LoggerService } from 'projects/logger/src/public-api';

import {
  MappReadingListActionTypes,
  RemoveMappReadingList,
  RemoveMappReadingListSuccess,
  RemoveMappReadingListFail,
  LoadMappReadingList,
  LoadMappReadingListSuccess,
  LoadMappReadingListFail,
  SelectMappReading,
  SelectMappReadingSuccess,
  SelectMappReadingFail,
  DeselectMappReading,
  DeselectMappReadingSuccess,
  DeselectMappReadingFail,
  AddNewMappReading,
  AddNewMappReadingSuccess,
  AddNewMappReadingFail,
  RemoveMappReading,
  RemoveMappReadingSuccess,
  RemoveMappReadingFail,
  CreateMappReading,
  CreateMappReadingSuccess,
  CreateMappReadingFail,
  UpdateMappReading,
  UpdateMappReadingSuccess,
  UpdateMappReadingFail
} from './mapp-reading-list.action';
import { IMappReadingStoreState } from '../mapp-reading-store.state';
import { getSelectedMappReading } from './mapp-reading-list.selector';
import { MappReadingService } from '../../service/mapp-reading.service';
import { MappReading } from '../../model/mapp-reading.model';

@Injectable()
export class MappReadingListEffects {
  namespace: string = 'MappReadingListEffects';

  constructor(
    private actions$: Actions,
    private store: Store<IMappReadingStoreState>,
    private mappReadingService: MappReadingService,
    private logger: LoggerService
  ) {}

  //#region LoadMappReadingList
  loadMappReadingList$ = createEffect(() => this.actions$.pipe(
    ofType<LoadMappReadingList>(MappReadingListActionTypes.LOAD_MAPPREADINGLIST),
    switchMap(action => {
      this.logger.info(`${this.namespace}.loadMappReadingList$`);
      return this.mappReadingService
        .getAll()
        .pipe(
          map(response => {
            return new LoadMappReadingListSuccess({
            mappReadingList: response
          })
        }),
          catchError(error => of(new LoadMappReadingListFail({
            error: error
          })))
        );
    })
  ));

  loadMappReadingListSuccess$ = createEffect(() => this.actions$.pipe(
    ofType<LoadMappReadingListSuccess>(MappReadingListActionTypes.LOAD_MAPPREADINGLIST_SUCCESS),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.loadMappReadingListSuccess$`);
    })
  ), { dispatch: false });
  
  loadMappReadingListFail$ = createEffect(() => this.actions$.pipe(
    ofType<LoadMappReadingListFail>(MappReadingListActionTypes.LOAD_MAPPREADINGLIST_FAIL),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.loadMappReadingListFail$`);
      throw new Error(`Failed to load the readings: ${payload.error.error.message}`);
    })
  ), { dispatch: false });
  //#endregion LoadMappReadingList

  //#region RemoveMappReadingList
  removeMappReadingList$ = createEffect(() => this.actions$.pipe(
    ofType<RemoveMappReadingList>(MappReadingListActionTypes.REMOVE_MAPPREADINGLIST),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.removeMappReadingListSuccess$ | Readings: ${payload.mappReadingList.length}`);
      return new RemoveMappReadingListSuccess({
        mappReadingList: payload.mappReadingList
      });
    })
  ));
  
  removeMappReadingListSuccess$ = createEffect(() => this.actions$.pipe(
    ofType<RemoveMappReadingListSuccess>(MappReadingListActionTypes.REMOVE_MAPPREADINGLIST_SUCCESS),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.removeMappReadingListSuccess$ | Readings: ${payload.mappReadingList.length}`);
    })
  ), { dispatch: false });
  
  removeMappReadingListFail$ = createEffect(() => this.actions$.pipe(
    ofType<RemoveMappReadingListFail>(MappReadingListActionTypes.REMOVE_MAPPREADINGLIST_FAIL),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.removeMappReadingListFail$ | Readings: ${payload.mappReadingList.length}`);
      throw new Error(`Failed to remove readings: ${payload.error.error.message}`);
    })
  ), { dispatch: false });
  //#endregion RemoveMappReadingList

  //#region SelectMappReading
  selectMappReading$ = createEffect(() => this.actions$.pipe(
    ofType<SelectMappReading>(MappReadingListActionTypes.SELECT_MAPPREADING),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.selectMappReading$ | Reading: ${payload.mappReading.id}`);
      return new SelectMappReadingSuccess({
        mappReading: payload.mappReading
      });
    })
  ));
  
  selectMappReadingSuccess$ = createEffect(() => this.actions$.pipe(
    ofType<SelectMappReadingSuccess>(MappReadingListActionTypes.SELECT_MAPPREADING_SUCCESS),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.selectMappReadingSuccess$ | Reading: ${payload.mappReading.id}`);
    })
  ), { dispatch: false });
  
  selectMappReadingFail$ = createEffect(() => this.actions$.pipe(
    ofType<SelectMappReadingFail>(MappReadingListActionTypes.SELECT_MAPPREADING_FAIL),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.selectMappReadingFail$ | Reading: ${payload.mappReading.id}`);
      throw new Error(`Failed to select the reading: ${payload.error.error.message}`);
    })
  ), { dispatch: false });
  //#endregion SelectMappReading

  //#region DeselectMappReading
  deselectMappReading$ = createEffect(() => this.actions$.pipe(
    ofType<DeselectMappReading>(MappReadingListActionTypes.DESELECT_MAPPREADING),
    withLatestFrom(
      this.store.select(getSelectedMappReading),
      (action, selectedMappReading) => ({
        selectedMappReading: selectedMappReading
      })
    ),
    map(payload => {
      this.logger.info(`${this.namespace}.deselectMappReading$`);
      if (payload.selectedMappReading) {
        if (payload.selectedMappReading.new === true) {
          this.store.dispatch(new RemoveMappReading({
            mappReadingId: payload.selectedMappReading.id
          }));
        }
      }
      return new DeselectMappReadingSuccess();
    })
  ));
  
  deselectMappReadingSuccess$ = createEffect(() => this.actions$.pipe(
    ofType<DeselectMappReadingSuccess>(MappReadingListActionTypes.DESELECT_MAPPREADING_SUCCESS),
    map(payload => {
      this.logger.info(`${this.namespace}.deselectMappReadingSuccess$`);
    })
  ), { dispatch: false });
  
  deselectMappReadingFail$ = createEffect(() => this.actions$.pipe(
    ofType<DeselectMappReadingFail>(MappReadingListActionTypes.DESELECT_MAPPREADING_FAIL),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.deselectMappReadingFail$`);
      throw new Error(`Failed to deselect the reading: ${payload.error.error.message}`);
    })
  ), { dispatch: false });
  //#endregion DeselectMappReading

  //#region AddNewMappReading
  addNewMappReading$ = createEffect(() => this.actions$.pipe(
    ofType<AddNewMappReading>(MappReadingListActionTypes.ADDNEW_MAPPREADING),
    map(action => {
      this.logger.info(`${this.namespace}.addNewMappReading$`);
      return new AddNewMappReadingSuccess({
        mappReading: new MappReading()
      });
    })
  ));

  addNewMappReadingSuccess$ = createEffect(() => this.actions$.pipe(
    ofType<AddNewMappReadingSuccess>(MappReadingListActionTypes.ADDNEW_MAPPREADING_SUCCESS),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.addNewMappReadingSuccess$ | Reading: ${payload.mappReading.id}`);
      return new SelectMappReading({
        mappReading: payload.mappReading
      });
    })
  ));
  
  addNewMappReadingFail$ = createEffect(() => this.actions$.pipe(
    ofType<AddNewMappReadingFail>(MappReadingListActionTypes.ADDNEW_MAPPREADING_FAIL),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.addNewMappReadingFail$`);
      throw new Error(`Failed to add a new reading: ${payload.error.error.message}`);
    })
  ), { dispatch: false });
  //#endregion AddNewMappReading

  //#region RemoveMappReading
  removeMappReading$ = createEffect(() => this.actions$.pipe(
    ofType<RemoveMappReading>(MappReadingListActionTypes.REMOVE_MAPPREADING),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.removeMappReading$ | Reading: ${payload.mappReadingId}`);
      return new RemoveMappReadingSuccess({
        mappReadingId: payload.mappReadingId
      });
    })
  ));

  removeMappReadingSuccess$ = createEffect(() => this.actions$.pipe(
    ofType<RemoveMappReadingSuccess>(MappReadingListActionTypes.REMOVE_MAPPREADING_SUCCESS),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.removeMappReadingSuccess$ | Reading: ${payload.mappReadingId}`);
    })
  ), { dispatch: false });
  
  removeMappReadingFail$ = createEffect(() => this.actions$.pipe(
    ofType<RemoveMappReadingFail>(MappReadingListActionTypes.REMOVE_MAPPREADING_FAIL),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.removeMappReadingFail$ | Reading: ${payload.mappReadingId}`);
      throw new Error(`Failed to remove the reading: ${payload.error.error.message}`);
    })
  ), { dispatch: false });
  //#endregion RemoveMappReading

  //#region CreateMappReading
  createMappReading$ = createEffect(() => this.actions$.pipe(
    ofType<CreateMappReading>(MappReadingListActionTypes.CREATE_MAPPREADING),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.createMappReading$ | Reading: ${payload.mappReading.id}`);
      //TODO: Call service here to create the reading if propogating on the server.
      return new CreateMappReadingSuccess({
        mappReading: Object.assign({}, payload.mappReading, {
          new: false,
          readingTs: new Date()
        })
      });
    })
  ));

  createMappReadingSuccess$ = createEffect(() => this.actions$.pipe(
    ofType<CreateMappReadingSuccess>(MappReadingListActionTypes.CREATE_MAPPREADING_SUCCESS),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.createMappReadingSuccess$ | Reading: ${payload.mappReading.id}`);
      // TODO: Notify the user of successful creation (toast, etc.)
      return new DeselectMappReading();
    })
  ));
  
  createMappReadingFail$ = createEffect(() => this.actions$.pipe(
    ofType<CreateMappReadingFail>(MappReadingListActionTypes.CREATE_MAPPREADING_FAIL),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.createMappReadingFail$`);
      throw new Error(`Failed to create the reading: ${payload.error.error.message}`);
    })
  ), { dispatch: false });
  //#endregion CreateMappReading

  //#region UpdateMappReading
  updateMappReading$ = createEffect(() => this.actions$.pipe(
    ofType<UpdateMappReading>(MappReadingListActionTypes.UPDATE_MAPPREADING),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.updateMappReading$ | Reading: ${payload.mappReading.id}`);
      //TODO: Call service here to create the reading if propogating on the server.
      return new UpdateMappReadingSuccess({
        mappReading: payload.mappReading
      });
    })
  ));

  updateMappReadingSuccess$ = createEffect(() => this.actions$.pipe(
    ofType<UpdateMappReadingSuccess>(MappReadingListActionTypes.UPDATE_MAPPREADING_SUCCESS),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.updateMappReadingSuccess$ | Reading: ${payload.mappReading.id}`);
      // TODO: Notify the user of successful update (toast, etc.)
      return new DeselectMappReading();
    })
  ));
  
  updateMappReadingFail$ = createEffect(() => this.actions$.pipe(
    ofType<UpdateMappReadingFail>(MappReadingListActionTypes.UPDATE_MAPPREADING_FAIL),
    map(action => action.payload),
    map(payload => {
      this.logger.info(`${this.namespace}.updateMappReadingFail$`);
      throw new Error(`Failed to update the reading: ${payload.error.error.message}`);
    })
  ), { dispatch: false });
  //#endregion UpdateMappReading
}
