import { Action } from '@ngrx/store';
import { IMappReading } from '../../model/mapp-reading.model';

//#region ActionTypes
export enum MappReadingListActionTypes {
  LOAD_MAPPREADINGLIST = '[mapp-reading-store] Load MappReadingList',
  LOAD_MAPPREADINGLIST_SUCCESS = '[mapp-reading-store] Load MappReadingList Success',
  LOAD_MAPPREADINGLIST_FAIL = '[mapp-reading-store] Load MappReadingList Fail',

  REMOVE_MAPPREADINGLIST = '[mapp-reading-store] Remove MappReadingList',
  REMOVE_MAPPREADINGLIST_SUCCESS = '[mapp-reading-store] Remove MappReadingList Success',
  REMOVE_MAPPREADINGLIST_FAIL = '[mapp-reading-store] Remove MappReadingList Fail',

  SELECT_MAPPREADING = '[mapp-reading-store] Select MappReading',
  SELECT_MAPPREADING_SUCCESS = '[mapp-reading-store] Select MappReading Success',
  SELECT_MAPPREADING_FAIL = '[mapp-reading-store] Select MappReading Fail',

  DESELECT_MAPPREADING = '[mapp-reading-store] Deselect MappReading',
  DESELECT_MAPPREADING_SUCCESS = '[mapp-reading-store] Deselect MappReading Success',
  DESELECT_MAPPREADING_FAIL = '[mapp-reading-store] Deselect MappReading Fail',

  ADDNEW_MAPPREADING = '[mapp-reading-store] AddNew MappReading',
  ADDNEW_MAPPREADING_SUCCESS = '[mapp-reading-store] AddNew MappReading Success',
  ADDNEW_MAPPREADING_FAIL = '[mapp-reading-store] AddNew MappReading Fail',

  REMOVE_MAPPREADING = '[mapp-reading-store] Remove MappReading',
  REMOVE_MAPPREADING_SUCCESS = '[mapp-reading-store] Remove MappReading Success',
  REMOVE_MAPPREADING_FAIL = '[mapp-reading-store] Remove MappReading Fail',

  CREATE_MAPPREADING = '[mapp-reading-store] Create MappReading',
  CREATE_MAPPREADING_SUCCESS = '[mapp-reading-store] Create MappReading Success',
  CREATE_MAPPREADING_FAIL = '[mapp-reading-store] Create MappReading Fail',

  UPDATE_MAPPREADING = '[mapp-reading-store] Update MappReading',
  UPDATE_MAPPREADING_SUCCESS = '[mapp-reading-store] Update MappReading Success',
  UPDATE_MAPPREADING_FAIL = '[mapp-reading-store] Update MappReading Fail'
}
//#endregion ActionTypes

//#region LoadMappReadingList
export class LoadMappReadingList implements Action {
  readonly type = MappReadingListActionTypes.LOAD_MAPPREADINGLIST;
  constructor() {}
}

export class LoadMappReadingListSuccess implements Action {
  readonly type = MappReadingListActionTypes.LOAD_MAPPREADINGLIST_SUCCESS;
  constructor(public payload: { mappReadingList: IMappReading[] }) {}
}

export class LoadMappReadingListFail implements Action {
  readonly type = MappReadingListActionTypes.LOAD_MAPPREADINGLIST_FAIL;
  constructor(public payload: { error: any }) {}
}
//#endregion LoadMappReadingList

//#region RemoveMappReadingList
export class RemoveMappReadingList implements Action {
  readonly type = MappReadingListActionTypes.REMOVE_MAPPREADINGLIST;
  constructor(public payload: { mappReadingList: IMappReading[] }) {}
}

export class RemoveMappReadingListSuccess implements Action {
  readonly type = MappReadingListActionTypes.REMOVE_MAPPREADINGLIST_SUCCESS;
  constructor(public payload: { mappReadingList: IMappReading[] }) {}
}

export class RemoveMappReadingListFail implements Action {
  readonly type = MappReadingListActionTypes.REMOVE_MAPPREADINGLIST_FAIL;
  constructor(public payload: { mappReadingList: IMappReading[], error: any }) {}
}
//#endregion RemoveMappReadingList

//#region SelectMappReading
export class SelectMappReading implements Action {
  readonly type = MappReadingListActionTypes.SELECT_MAPPREADING;
  constructor(public payload: { mappReading: IMappReading }) {}
}

export class SelectMappReadingSuccess implements Action {
  readonly type = MappReadingListActionTypes.SELECT_MAPPREADING_SUCCESS;
  constructor(public payload: { mappReading: IMappReading }) {}
}

export class SelectMappReadingFail implements Action {
  readonly type = MappReadingListActionTypes.SELECT_MAPPREADING_FAIL;
  constructor(public payload: { mappReading: IMappReading, error: any }) {}
}
//#endregion SelectMappReading

//#region DeselectMappReading
export class DeselectMappReading implements Action {
  readonly type = MappReadingListActionTypes.DESELECT_MAPPREADING;
  constructor() {}
}

export class DeselectMappReadingSuccess implements Action {
  readonly type = MappReadingListActionTypes.DESELECT_MAPPREADING_SUCCESS;
  constructor() {}
}

export class DeselectMappReadingFail implements Action {
  readonly type = MappReadingListActionTypes.DESELECT_MAPPREADING_FAIL;
  constructor(public payload: { error: any }) {}
}
//#endregion DeselectMappReading

//#region AddNewMappReading
export class AddNewMappReading implements Action {
  readonly type = MappReadingListActionTypes.ADDNEW_MAPPREADING;
  constructor() {}
}

export class AddNewMappReadingSuccess implements Action {
  readonly type = MappReadingListActionTypes.ADDNEW_MAPPREADING_SUCCESS;
  constructor(public payload: { mappReading: IMappReading }) {}
}

export class AddNewMappReadingFail implements Action {
  readonly type = MappReadingListActionTypes.ADDNEW_MAPPREADING_FAIL;
  constructor(public payload: { error: any }) {}
}
//#endregion AddNewMappReading

//#region RemoveMappReading
export class RemoveMappReading implements Action {
  readonly type = MappReadingListActionTypes.REMOVE_MAPPREADING;
  constructor(public payload: { mappReadingId: string }) {}
}

export class RemoveMappReadingSuccess implements Action {
  readonly type = MappReadingListActionTypes.REMOVE_MAPPREADING_SUCCESS;
  constructor(public payload: { mappReadingId: string }) {}
}

export class RemoveMappReadingFail implements Action {
  readonly type = MappReadingListActionTypes.REMOVE_MAPPREADING_FAIL;
  constructor(public payload: { mappReadingId: string, error: any }) {}
}
//#endregion RemoveMappReading

//#region CreateMappReading
export class CreateMappReading implements Action {
  readonly type = MappReadingListActionTypes.CREATE_MAPPREADING;
  constructor(public payload: { mappReading: IMappReading }) {}
}

export class CreateMappReadingSuccess implements Action {
  readonly type = MappReadingListActionTypes.CREATE_MAPPREADING_SUCCESS;
  constructor(public payload: { mappReading: IMappReading }) {}
}

export class CreateMappReadingFail implements Action {
  readonly type = MappReadingListActionTypes.CREATE_MAPPREADING_FAIL;
  constructor(public payload: { mappReading: IMappReading, error: any }) {}
}
//#endregion CreateMappReading

//#region UpdateMappReading
export class UpdateMappReading implements Action {
  readonly type = MappReadingListActionTypes.UPDATE_MAPPREADING;
  constructor(public payload: { mappReading: IMappReading }) {}
}

export class UpdateMappReadingSuccess implements Action {
  readonly type = MappReadingListActionTypes.UPDATE_MAPPREADING_SUCCESS;
  constructor(public payload: { mappReading: IMappReading }) {}
}

export class UpdateMappReadingFail implements Action {
  readonly type = MappReadingListActionTypes.UPDATE_MAPPREADING_FAIL;
  constructor(public payload: { mappReading: IMappReading, error: any }) {}
}
//#endregion UpdateMappReading

//#region Actions
export type MappReadingListActions =
  LoadMappReadingList
  | LoadMappReadingListSuccess
  | LoadMappReadingListFail  
  | RemoveMappReadingList
  | RemoveMappReadingListSuccess
  | RemoveMappReadingListFail
  | SelectMappReading
  | SelectMappReadingSuccess
  | SelectMappReadingFail
  | DeselectMappReading
  | DeselectMappReadingSuccess
  | DeselectMappReadingFail
  | AddNewMappReading
  | AddNewMappReadingSuccess
  | AddNewMappReadingFail
  | RemoveMappReading
  | RemoveMappReadingSuccess
  | RemoveMappReadingFail
  | CreateMappReading
  | CreateMappReadingSuccess
  | CreateMappReadingFail
  | UpdateMappReading
  | UpdateMappReadingSuccess
  | UpdateMappReadingFail;
//#endregion Actions
