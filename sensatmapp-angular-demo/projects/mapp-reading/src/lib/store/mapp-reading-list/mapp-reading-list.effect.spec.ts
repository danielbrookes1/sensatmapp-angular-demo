import { Observable, empty, of } from 'rxjs';
import { Actions } from '@ngrx/effects';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { hot, cold } from 'jasmine-marbles';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { StoreModule, MetaReducer } from '@ngrx/store';
import { MappReading, IMappReading } from '../../model/mapp-reading.model';
import { MappReadingStoreReducers } from '../mapp-reading-store.reducer';
import { MappReadingListEffects } from './mapp-reading-list.effect';
import {
  DeselectMappReading,
  DeselectMappReadingSuccess,
  RemoveMappReadingList,
  RemoveMappReadingListSuccess,
  SelectMappReading,
  SelectMappReadingSuccess,
} from './mapp-reading-list.action';
import { MappReadingService } from '../../service/mapp-reading.service';

export class TestActions extends Actions {
  constructor() {
    super(empty());
  }

  set stream(source: Observable<any>) {
    this.source = source;
  }
}

export function getActions() {
  return new TestActions();
}

export const metaReducers: MetaReducer<any>[] = [];

describe('MappReadingListEffects', () => {
  let actions$: TestActions;
  let service: MappReadingService;
  let effects: MappReadingListEffects;

  // TODO: Make these dummy readings more realistic
  const mappReadingList: IMappReading[] = [
    Object.assign({}, new MappReading(), {
      readId: '56468045-7302-0a02-080e-ebf741819498',
      value: 10
    }),
    Object.assign({}, new MappReading(), {
      readId: '0d17a6bf-1642-53b1-a942-c01d3977293d',
      value: 100
    }),
    Object.assign({}, new MappReading(), {
      readId: 'd450c7a6-023c-3cfd-814d-776c0ba24d63',
      value: 1000
    })
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        MatSnackBarModule,
        StoreModule.forRoot(MappReadingStoreReducers, { metaReducers })
      ],
      providers: [
        MappReadingListEffects,
        { provide: Actions, useFactory: getActions }
      ],
    });

    actions$ = TestBed.get(Actions);
    service = TestBed.get(MappReadingService);
    effects = TestBed.get(MappReadingListEffects);

    spyOn(service, 'getAll').and.returnValue(of(mappReadingList));
  });

  describe('RemoveMappReadingList$', () => {
    it('should remove a collection of process version items', () => {
      const action = new RemoveMappReadingList({
        mappReadingList
      });
      const completion = new RemoveMappReadingListSuccess({
        mappReadingList
      });

      actions$.stream = hot('-a', { a: action });
      const expected = cold('-b', { b: completion });

      expect(effects.removeMappReadingList$).toBeObservable(expected);
    });
  });

  describe('SelectMappReading$', () => {
    it('should update the selected process version', () => {
      const action = new SelectMappReading({
        mappReading: mappReadingList[0]
      });
      const completion = new SelectMappReadingSuccess({
        mappReading: mappReadingList[0]
      });

      actions$.stream = hot('-a', { a: action });
      const expected = cold('-b', { b: completion });

      expect(effects.selectMappReading$).toBeObservable(expected);
    });
  });

  describe('DeselectMappReading$', () => {
    it('should set the selected process version to null', () => {
      const action = new DeselectMappReading();
      const completion = new DeselectMappReadingSuccess();

      actions$.stream = hot('-a', { a: action });
      const expected = cold('-b', { b: completion });

      expect(effects.deselectMappReading$).toBeObservable(expected);
    });
  });

});

