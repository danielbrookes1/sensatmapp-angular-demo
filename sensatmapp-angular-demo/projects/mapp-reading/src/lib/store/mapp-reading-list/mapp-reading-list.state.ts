import { EntityAdapter, createEntityAdapter, EntityState } from '@ngrx/entity';
import { IMappReading } from '../../model/mapp-reading.model';

export const mappReadingListAdapter: EntityAdapter<IMappReading> = createEntityAdapter<IMappReading>({
  selectId: model => model.id,
  sortComparer: (a: IMappReading, b: IMappReading): number =>
    a.readingTs === b.readingTs ? a.dataType.toString().localeCompare(b.dataType.toString()) : a.readingTs > b.readingTs ? 1 : -1
  });

export interface MappReadingListState extends EntityState<IMappReading> {
  loading: boolean;
  loaded: boolean;

  removing: boolean;
  removed: boolean;

  selecting: boolean;
  selected: boolean;

  deselecting: boolean;
  deselected: boolean;

  adding: boolean;
  added: boolean;

  creating: boolean;
  created: boolean;

  updating: boolean;
  updated: boolean;

  refreshed: Date;
  selectedMappReadingId: string;
}

export const initialMappReadingListState: MappReadingListState = mappReadingListAdapter.getInitialState({
  loading: false,
  loaded: false,
  
  removing: false,
  removed: false,

  selecting: false,
  selected: false,

  deselecting: false,
  deselected: false,

  adding: false,
  added: false,

  creating: false,
  created: false,

  updating: false,
  updated: false,

  refreshed: null,
  selectedMappReadingId: null
});
