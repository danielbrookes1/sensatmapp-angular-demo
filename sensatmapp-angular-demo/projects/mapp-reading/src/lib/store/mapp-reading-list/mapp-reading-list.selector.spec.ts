import { StoreModule, Store } from '@ngrx/store';

import { TestBed } from '@angular/core/testing';
import { MappReading, IMappReading } from '../../model/mapp-reading.model';

import { mappReadingListReducer } from './mapp-reading-list.reducer';
import { IMappReadingStoreState } from '../mapp-reading-store.state';

describe('MappReadingList Selectors', () => {
  let store: Store<IMappReadingStoreState>;
  // TODO: Make these dummy readings more realistic
  const mappReadingList: IMappReading[] = [
    Object.assign({}, new MappReading(), {
      readId: '56468045-7302-0a02-080e-ebf741819498',
      value: 10
    }),
    Object.assign({}, new MappReading(), {
      readId: '0d17a6bf-1642-53b1-a942-c01d3977293d',
      value: 100
    }),
    Object.assign({}, new MappReading(), {
      readId: 'd450c7a6-023c-3cfd-814d-776c0ba24d63',
      value: 1000
    })
  ];

  const entities = {
    1: mappReadingList[0],
    2: mappReadingList[1],
    3: mappReadingList[2],
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          ...mappReadingListReducer
        }),
      ],
    });

    store = TestBed.get(Store);
  });

});
