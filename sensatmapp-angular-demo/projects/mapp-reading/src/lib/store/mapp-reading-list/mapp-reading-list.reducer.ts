import { MappReadingListActions, MappReadingListActionTypes } from './mapp-reading-list.action';
import { MappReadingListState, initialMappReadingListState, mappReadingListAdapter } from './mapp-reading-list.state';

export function mappReadingListReducer(
  state = initialMappReadingListState,
  action: MappReadingListActions
): MappReadingListState {
  switch (action.type) {
    case MappReadingListActionTypes.LOAD_MAPPREADINGLIST: return { ...state, loading: true, loaded: false };
    case MappReadingListActionTypes.LOAD_MAPPREADINGLIST_SUCCESS: return mappReadingListAdapter.setAll(action.payload.mappReadingList, { ...state, loading: false, loaded: true, refreshed: new Date() });
    case MappReadingListActionTypes.LOAD_MAPPREADINGLIST_FAIL: return { ...state, loading: false, loaded: false };
    
    case MappReadingListActionTypes.REMOVE_MAPPREADINGLIST: return { ...state, removing: true, removed: false };
    case MappReadingListActionTypes.REMOVE_MAPPREADINGLIST_SUCCESS: return mappReadingListAdapter.removeMany(action.payload.mappReadingList.map(o => o.id), { ...state, removing: false, removed: true });
    case MappReadingListActionTypes.REMOVE_MAPPREADINGLIST_FAIL: return { ...state, removing: false, removed: false };

    case MappReadingListActionTypes.SELECT_MAPPREADING: return { ...state, selecting: true, selected: false, selectedMappReadingId: null };
    case MappReadingListActionTypes.SELECT_MAPPREADING_SUCCESS: return { ...state, selecting: false, selected: true, selectedMappReadingId: action.payload.mappReading.id };
    case MappReadingListActionTypes.SELECT_MAPPREADING_FAIL: return { ...state, selecting: false, selected: false };

    case MappReadingListActionTypes.DESELECT_MAPPREADING: return { ...state, deselecting: true, deselected: false };
    case MappReadingListActionTypes.DESELECT_MAPPREADING_SUCCESS: return { ...state, deselecting: false, deselected: true, selectedMappReadingId: null };
    case MappReadingListActionTypes.DESELECT_MAPPREADING_FAIL: return { ...state, deselecting: false, deselected: false };

    case MappReadingListActionTypes.ADDNEW_MAPPREADING: return { ...state, adding: true, added: false };
    case MappReadingListActionTypes.ADDNEW_MAPPREADING_SUCCESS: return mappReadingListAdapter.addOne(action.payload.mappReading, {...state, adding: false, added: true });
    case MappReadingListActionTypes.ADDNEW_MAPPREADING_FAIL: return { ...state, adding: false, added: false };

    case MappReadingListActionTypes.REMOVE_MAPPREADING: return { ...state, removing: true, removed: false };
    case MappReadingListActionTypes.REMOVE_MAPPREADING_SUCCESS: return mappReadingListAdapter.removeOne(action.payload.mappReadingId, { ...state, removing: false, removed: true });
    case MappReadingListActionTypes.REMOVE_MAPPREADING_FAIL: return { ...state, removing: false, removed: false };

    case MappReadingListActionTypes.CREATE_MAPPREADING: return { ...state, creating: true, created: false };
    case MappReadingListActionTypes.CREATE_MAPPREADING_SUCCESS: return mappReadingListAdapter.updateOne({ id: action.payload.mappReading.id, changes: action.payload.mappReading }, { ...state, creating: false, created: true });
    case MappReadingListActionTypes.CREATE_MAPPREADING_FAIL: return { ...state, creating: false, created: false };

    case MappReadingListActionTypes.UPDATE_MAPPREADING: return { ...state, updating: true, updated: false };
    case MappReadingListActionTypes.UPDATE_MAPPREADING_SUCCESS: return mappReadingListAdapter.updateOne({ id: action.payload.mappReading.id, changes: action.payload.mappReading }, { ...state, updating: false, updated: true });
    case MappReadingListActionTypes.UPDATE_MAPPREADING_FAIL: return { ...state, updating: false, updated: false };

    default: return state;
  }
}

export const { selectAll, selectEntities, selectIds, selectTotal } = mappReadingListAdapter.getSelectors();
