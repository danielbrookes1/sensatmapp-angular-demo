import { 
  MappReadingListActionTypes,
  RemoveMappReadingList,
  RemoveMappReadingListSuccess,
  RemoveMappReadingListFail,
  SelectMappReading,
  SelectMappReadingSuccess,
  SelectMappReadingFail,
  DeselectMappReading,
  DeselectMappReadingSuccess,
  DeselectMappReadingFail,
  AddNewMappReading,
  AddNewMappReadingSuccess,
  AddNewMappReadingFail,
  CreateMappReading,
  CreateMappReadingSuccess,
  CreateMappReadingFail,
  UpdateMappReading,
  UpdateMappReadingSuccess,
  UpdateMappReadingFail
} from './mapp-reading-list.action';
import { MappReading, IMappReading } from '../../model/mapp-reading.model';

describe('MappReadingList Actions', () => {
  // TODO: Make these dummy readings more realistic
  const mappReadingList: IMappReading[] = [
    Object.assign({}, new MappReading(), {
      readId: '56468045-7302-0a02-080e-ebf741819498',
      value: 10
    }),
    Object.assign({}, new MappReading(), {
      readId: '0d17a6bf-1642-53b1-a942-c01d3977293d',
      value: 100
    }),
    Object.assign({}, new MappReading(), {
      readId: 'd450c7a6-023c-3cfd-814d-776c0ba24d63',
      value: 1000
    })
  ];

  describe('RemoveMappReadingList Action', () => {

    describe('RemoveMappReadingList', () => {
      it('should create an action', () => {
        const payload = {
          mappReadingList
        };
        const action = new RemoveMappReadingList(payload);
        expect({...action}).toEqual({
          type: MappReadingListActionTypes.REMOVE_MAPPREADINGLIST,
          payload
        });
      });
    });

    describe('RemoveMappReadingList Success', () => {
      it('should create an action', () => {
        const payload = {
          mappReadingList
        };
        const action = new RemoveMappReadingListSuccess(payload);
        expect({...action}).toEqual({
          type: MappReadingListActionTypes.REMOVE_MAPPREADINGLIST_SUCCESS,
          payload
        });
      });
    });

    describe('RemoveMappReadingList Fail', () => {
      it('should create an action', () => {
        const payload = {
          mappReadingList,
          error: {
            error: {
              message: 'An error occurred.'
            }
          }
        };
        const action = new RemoveMappReadingListFail(payload);
        expect({...action}).toEqual({
          type: MappReadingListActionTypes.REMOVE_MAPPREADINGLIST_FAIL,
          payload
        });
      });
    });

  });

  describe('SelectMappReading Action', () => {

    describe('SelectMappReading', () => {
      it('should create an action', () => {
        const payload = {
          mappReading: mappReadingList[0]
        };
        const action = new SelectMappReading(payload);
        expect({...action}).toEqual({
          type: MappReadingListActionTypes.SELECT_MAPPREADING
        });
      });
    });

    describe('SelectMappReading Success', () => {
      it('should create an action', () => {
        const payload = {
          mappReading: mappReadingList[0]
        };
        const action = new SelectMappReadingSuccess(payload);
        expect({...action}).toEqual({
          type: MappReadingListActionTypes.SELECT_MAPPREADING_SUCCESS,
          payload
        });
      });
    });

    describe('SelectMappReading Fail', () => {
      it('should create an action', () => {
        const payload = {
          mappReading: mappReadingList[0],
          error: {
            error: {
              message: 'An error occurred.'
            }
          }
        };
        const action = new SelectMappReadingFail(payload);
        expect({...action}).toEqual({
          type: MappReadingListActionTypes.SELECT_MAPPREADING_FAIL,
          payload
        });
      });
    });

  });

  describe('DeselectMappReading Action', () => {

    describe('DeselectMappReading', () => {
      it('should create an action', () => {
        const action = new DeselectMappReading();
        expect({...action}).toEqual({
          type: MappReadingListActionTypes.DESELECT_MAPPREADING
        });
      });
    });

    describe('DeselectMappReading Success', () => {
      it('should create an action', () => {
        const action = new DeselectMappReadingSuccess();
        expect({...action}).toEqual({
          type: MappReadingListActionTypes.DESELECT_MAPPREADING_SUCCESS
        });
      });
    });

    describe('DeselectMappReading Fail', () => {
      it('should create an action', () => {
        const payload = {
          error: {
            error: {
              message: 'An error occurred.'
            }
          }
        };
        const action = new DeselectMappReadingFail(payload);
        expect({...action}).toEqual({
          type: MappReadingListActionTypes.DESELECT_MAPPREADING_FAIL,
          payload
        });
      });
    });

  });

  describe('AddNewMappReading Actions', () => {
    describe('AddNewMappReading', () => {
      it('should create an action', () => {
        const action = new AddNewMappReading();
        expect({...action}).toEqual({
          type: MappReadingListActionTypes.ADDNEW_MAPPREADING
        });
      });
    });

    describe('AddNewMappReadingSuccess', () => {
      it('should create an action', () => {
        const payload = {
          mappReading: mappReadingList[0]
        };
        const action = new AddNewMappReadingSuccess(payload);
        expect({...action}).toEqual({
          type: MappReadingListActionTypes.ADDNEW_MAPPREADING_SUCCESS,
          payload
        });
      });
    });

    describe('AddNewMappReadingFail', () => {
      it('should create an action', () => {
        const payload = {
          error: {
            error: {
              message: 'An error occurred.'
            }
          }
        };
        const action = new AddNewMappReadingFail(payload);
        expect({...action}).toEqual({
          type: MappReadingListActionTypes.ADDNEW_MAPPREADING_FAIL,
          payload
        });
      });
    });
  });

  describe('CreateMappReading Actions', () => {
    describe('CreateMappReading', () => {
      it('should create an action', () => {
        const payload = {
          mappReading: mappReadingList[0]
        };
        const action = new CreateMappReading(payload);
        expect({...action}).toEqual({
          type: MappReadingListActionTypes.CREATE_MAPPREADING,
          payload
        });
      });
    });

    describe('CreateMappReadingSuccess', () => {
      it('should create an action', () => {
        const payload = {
          mappReading: mappReadingList[0]
        };
        const action = new CreateMappReadingSuccess(payload);
        expect({...action}).toEqual({
          type: MappReadingListActionTypes.CREATE_MAPPREADING_SUCCESS,
          payload
        });
      });
    });

    describe('CreateMappReadingFail', () => {
      it('should create an action', () => {
        const payload = {
          mappReading: mappReadingList[0],
          error: {
            error: {
              message: 'An error occurred.'
            }
          }
        };
        const action = new CreateMappReadingFail(payload);
        expect({...action}).toEqual({
          type: MappReadingListActionTypes.CREATE_MAPPREADING_FAIL,
          payload
        });
      });
    });
  });

  describe('UpdateMappReading Actions', () => {
    describe('UpdateMappReading', () => {
      it('should create an action', () => {
        const payload = {
          mappReading: mappReadingList[0]
        };
        const action = new UpdateMappReading(payload);
        expect({...action}).toEqual({
          type: MappReadingListActionTypes.UPDATE_MAPPREADING,
          payload
        });
      });
    });

    describe('UpdateMappReadingSuccess', () => {
      it('should create an action', () => {
        const payload = {
          mappReading: mappReadingList[0]
        };
        const action = new UpdateMappReadingSuccess(payload);
        expect({...action}).toEqual({
          type: MappReadingListActionTypes.UPDATE_MAPPREADING_SUCCESS,
          payload
        });
      });
    });

    describe('UpdateMappReadingFail', () => {
      it('should create an action', () => {
        const payload = {
          mappReading: mappReadingList[0],
          error: {
            error: {
              message: 'An error occurred.'
            }
          }
        };
        const action = new UpdateMappReadingFail(payload);
        expect({...action}).toEqual({
          type: MappReadingListActionTypes.UPDATE_MAPPREADING_FAIL,
          payload
        });
      });
    });
  });

});
