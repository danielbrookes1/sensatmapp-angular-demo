import { Type } from '@angular/core';
import { MappReadingListEffects } from './mapp-reading-list/mapp-reading-list.effect';

export const MappReadingStoreEffects: Type<any>[] = [
  MappReadingListEffects
];
