import { createFeatureSelector } from '@ngrx/store';
import { MappReadingListState } from './mapp-reading-list/mapp-reading-list.state';

export interface IMappReadingStoreState {
  mappReadingList: MappReadingListState;
}

export const getInitialMappReadingStoreState = createFeatureSelector<IMappReadingStoreState>(
  'mapp-reading-store'
);
