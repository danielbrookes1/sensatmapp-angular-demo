import { ActionReducerMap } from '@ngrx/store';
import { IMappReadingStoreState } from './mapp-reading-store.state';
import { mappReadingListReducer } from './mapp-reading-list/mapp-reading-list.reducer';

export const MappReadingStoreReducers: ActionReducerMap<IMappReadingStoreState, any> = {
  mappReadingList: mappReadingListReducer
};
