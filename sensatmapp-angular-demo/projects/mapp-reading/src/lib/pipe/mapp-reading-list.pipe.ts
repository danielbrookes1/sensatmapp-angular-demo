import { Injectable } from '@angular/core';
import { Pipe, PipeTransform } from '@angular/core';
import { IMappReading } from '../model/mapp-reading.model';

@Injectable({
  providedIn: 'root'
})

@Pipe({
  name: 'filterMappReadingListByNew',
  pure: false
})

export class FilterMappReadingListByNewPipe implements PipeTransform {
  transform(mappReadingList: IMappReading[], isNew: boolean = true): IMappReading[] {
    return mappReadingList ? mappReadingList.filter(o => o.new === isNew) : [];
  }
}

@Pipe({
  name: 'filterMappReadingListBySearchString',
  pure: false
})

export class FilterMappReadingListBySearchStringPipe implements PipeTransform {
  transform(mappReadingList: IMappReading[], searchString: string): IMappReading[] {
    return mappReadingList ? mappReadingList.filter(o => o.sensorType.toLowerCase().includes(searchString.toLowerCase()) || o.dataType.toLowerCase().includes(searchString.toLowerCase()) ) : [];
  }
}
