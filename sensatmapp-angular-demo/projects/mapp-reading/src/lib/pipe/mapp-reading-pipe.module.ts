import { NgModule }	from '@angular/core';
import {
  FilterMappReadingListByNewPipe, FilterMappReadingListBySearchStringPipe
} from './mapp-reading-list.pipe';

@NgModule({
  imports: [

  ],
  declarations: [
    FilterMappReadingListByNewPipe,
    FilterMappReadingListBySearchStringPipe
  ],
  exports: [
    FilterMappReadingListByNewPipe,
    FilterMappReadingListBySearchStringPipe
  ]
})
export class MappReadingPipeModule {}
