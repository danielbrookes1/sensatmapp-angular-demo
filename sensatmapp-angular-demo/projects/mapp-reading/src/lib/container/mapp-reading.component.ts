import { Component, Input, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Store } from '@ngrx/store';
import { LoggerService } from 'projects/logger/src/public-api';
import { IMappReadingStoreState } from '../store/mapp-reading-store.state';
import { IMappReading } from '../model/mapp-reading.model';
import {
  getMappReadingListItems,
  getMappReadingListLoaded,
  getMappReadingListLoading,
  getMappReadingListRefreshed,
  getSelectedMappReading
} from '../store/mapp-reading-store.selector';
import {
  AddNewMappReading,
  CreateMappReading,
  DeselectMappReading,
  LoadMappReadingList,
  SelectMappReading,
  UpdateMappReading,
  RemoveMappReading
} from '../store/mapp-reading-store.action';

@Component({
  selector: 'cmp-mapp-reading',
  templateUrl: './mapp-reading.component.html',
  styleUrls: ['./mapp-reading.component.scss']
})

export class MappReadingComponent implements OnInit {
  namespace: string = 'MappReadingComponent';

  mappReadingList$: Observable<IMappReading[]>;
  mappReadingListLoading$: Observable<boolean>;
  mappReadingListLoaded$: Observable<boolean>;
  mappReadingListRefreshed$: Observable<Date>;
  mappReading$: Observable<IMappReading>;

  constructor(
    private store: Store<IMappReadingStoreState>,
    private logger: LoggerService
  ) {
    this.logger.info(`${this.namespace}.ctor`);
  }

  ngOnInit() {
    this.logger.info(`${this.namespace}.ngOnInit`);
    this.mappReadingList$ = this.store.select(getMappReadingListItems);
    this.mappReadingListLoading$ = this.store.select(getMappReadingListLoading);
    this.mappReadingListLoaded$ = this.store.select(getMappReadingListLoaded);
    this.mappReadingListRefreshed$ = this.store.select(getMappReadingListRefreshed);
    this.mappReading$ = this.store.select(getSelectedMappReading);
  }

  onAddNew() {
    this.logger.info(`${this.namespace}.onAddNew`);
    this.store.dispatch(new AddNewMappReading());
  }

  onSelect(mappReading: IMappReading) {
    this.logger.info(`${this.namespace}.onSelect | Reading: ${mappReading.id}`);
    this.store.dispatch(new SelectMappReading({
      mappReading: mappReading
    }));
  }

  onRefresh() {
    this.logger.info(`${this.namespace}.onRefresh`);
    this.store.dispatch(new LoadMappReadingList());
  }

  onDeselect() {
    this.logger.info(`${this.namespace}.onDeselect`);
    this.store.dispatch(new DeselectMappReading());
  }

  onCreate(mappReading: IMappReading) {
    this.logger.info(`${this.namespace}.onCreate | Reading: ${mappReading.id}`);
    this.store.dispatch(new CreateMappReading({
      mappReading: mappReading
    }));
  }

  onUpdate(mappReading: IMappReading) {
    this.logger.info(`${this.namespace}.onUpdate | Reading: ${mappReading.id}`);
    this.store.dispatch(new UpdateMappReading({
      mappReading: mappReading
    }));
  }

  onRemove(mappReading: IMappReading) {
    this.logger.info(`${this.namespace}.onRemove | Reading: ${mappReading.id}`);
    this.store.dispatch(new RemoveMappReading({
      mappReadingId: mappReading.id
    }));
  }

}
