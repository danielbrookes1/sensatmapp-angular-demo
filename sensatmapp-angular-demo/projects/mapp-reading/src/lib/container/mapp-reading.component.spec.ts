import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { MappReadingStoreReducers } from '../store/mapp-reading-store.reducer';
import { MappReadingComponent } from './mapp-reading.component';
import { EffectsModule } from '@ngrx/effects';
import { MappReadingStoreEffects } from '../store/mapp-reading-store.effect';

describe('MappReadingComponent', () => {
  let component: MappReadingComponent;
  let fixture: ComponentFixture<MappReadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot([], { useHash: true }),
        StoreModule.forFeature('mapp-reading-store', MappReadingStoreReducers),
        EffectsModule.forFeature(MappReadingStoreEffects)
      ],
      declarations: [

      ],
      providers: [
        
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MappReadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
