import { UUID } from "angular2-uuid";
import { IMappReadingResponse } from "./response/mapp-reading-response.model";

export interface IMappReading {
  /**
   * Globally Unique Identifier (internal)
   */
  id: string;

  /**
   * Gets/sets whether the reading has been saved
   */
  new: boolean;

  /**
   * UUID for this reading
   */
  readId: string;

  /**
   * UUID of the box/container
   */
  containerId: string;
  
  /**
   * Type of the sensor
   */
  sensorType: string;

  /**
   * Type of data read by sensor
   */
  dataType: string;

  /**
   * Measuring range lower bound
   */
  rangeLower: number;

  /**
   * Measuring range upper bound
   */
  rangeUpper: number;

  /**
   * Location of the box (long)
   */
  longitude: number;

  /**
   * Location of the box (Lat)
   */
  latitude: number;

  /**
   * Actual value being read
   */
  value: number;

  /**
   * Measurement unit
   */
  unitType: string;

  /**
   * Timestamp of the reading
   */
  readingTs: Date;
}

export class MappReading implements IMappReading {
  id: string;
  new: boolean = true;
  readId: string;
  containerId: string;
  sensorType: string;
  dataType: string;
  rangeLower: number;
  rangeUpper: number;
  longitude: number;
  latitude: number;
  value: number;
  unitType: string;
  readingTs: Date;

  constructor(mappReadingResponse?: IMappReadingResponse) {
    this.id = UUID.UUID();
    if (mappReadingResponse) {
      this.new = false;
      this.readId = mappReadingResponse.id;
      this.containerId = mappReadingResponse.box_id;
      this.sensorType = mappReadingResponse.sensor_type;
      this.unitType = mappReadingResponse.unit;
      this.dataType = mappReadingResponse.name;
      this.rangeLower = mappReadingResponse.range_l;
      this.rangeUpper = mappReadingResponse.range_u;
      this.longitude = mappReadingResponse.longitude;
      this.latitude = mappReadingResponse.latitude;
      this.value = mappReadingResponse.reading;
      this.readingTs = mappReadingResponse.reading_ts;
    } else {
      this.readId = UUID.UUID();
    }
  }
}
